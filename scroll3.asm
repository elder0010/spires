base		equ	$3000	; Base location
scrolltext	equ $0400	; Test

; Zero page
cnt			equ	$1c
xpos		equ	$1d
ypos		equ	$1e
zeroval		equ	$1f
scrollbeg	equ $20		; Puntatore lo/hi per lo scroll (Fix)
scrollpos	equ $22		; Puntatore lo/hi per lo scroll
bgcolorfx	equ	$24		; Bordo nelle altre zone
fgcolorfx	equ	$25		; Background nelle altre zone
bgcolor		equ	$26		; Bordo in zona FX
fgcolor		equ	$27		; Background in zona FX
nextsprite	equ	$28		; indice prossimo sprite
bgline		equ	$29		; Colore linea decorativa (zeropage!)
waitzp		equ	$2b		; Indirizzo zeropage per wait da 3 o 5 
temp		equ	$2c		; Uso generico
temp1		equ	$2d		; Uso generico
mirror		equ	$2e		; Usato negli sprite per determinare quale met� del charset usare
spritespd	equ	$2f		; Velocit� dello scroll
; Absolute
waitbyte	equ	$0101	; Indirizzo assoluto per wait da 4 o 6
idlebyte	equ	$3fff	; Idle byte
nextirq		equ	lower2	; IRQ da cedere al controllo del tech-tech

; Inizializzazione

*=base
	jmp initall		; Inizializzazione generale, bisogna chiamarlo all'inizio
	jmp getirq		; restituisce in A il raster line a cui impostare l'irq e in Y,X Hi e Lo byte dell'handler
	jmp setirq		; configura l'irq di fine effetto al raster A ed imposta l'handler a Y,X Hi e Lo byte
	jmp inittext	; A Low/X High puntatore al testo (0 terminated)
	jmp setcolorfx	; Imposta colori A=Border color in zona effetto, X=Background color in zona effetto	
	jmp setcolor	; Imposta colori A=Border color in zona effetto, X=Background color fuori zona effetto	
	jmp setfx		; Accumulator: 0->no fx (background color), 1->square, 2->space, 3->both 	
	asl nextaction	; Carry: 0 no trigger, 1 trigger
	rts		

;align $0080

; Gestione interrupt presincronizzazione
; **************************************
lower2
	lsr $d019
	inc $d012	
	lda #<idlefx
	sta $fffe	
	lda #>idlefx
	sta $ffff
	lda #$c8
	sta $d016
	dec waitbyte
	dec waitbyte
	cli
	nop
	nop
	nop
	brk

; Gestione interrupt sincronizzato
; ********************************
idlefx
	ldy xpos
	ldx sine,y
	lda bgline	
	lsr
	and #$0f
	tay	
	lda blockpointer,x	
	sta self0+1
	sta self1+1		
	lda xpos
	lsr
	and #$07		
	ldx linefade,y	
	ldy $d012
	cpy $d012
	beq	sync
sync
	stx $d020
	stx $d021	
	tax
	lda block2pointer,x
	sta self01+1	
	clc
	adc #$01
	sta self11+1	
	ldy ypos
	ldx cosine,y	
	ldy #$01	
	lda zeroval
self0
	lda block,x	
self01	
	ora block2,y		
	ldy bgcolorfx		
	sty $d020
	ldy fgcolorfx
	sty $d021		
	ldy #$01
	sty cnt	
	inc waitzp	
	sta idlebyte	
idleloop	
	inx				; 2
	lda #$07		; 2
	sbx #$00		; 2		
	cpy #$05		; 2		
	tya				; 2
	and #$07		; 2
	tay				; 2	
	lda #$00
	bcs	self1		; 2/3
	sta waitbyte	; w4
	bit waitzp		; w3
	inc waitbyte	; w6
self1
	lda block,x		; 4	
self11
	ora block2,y	; 4	
	sta idlebyte	; 4		
	inc cnt
	ldy cnt	
	cpy #$1a		; 2
	bcc idleloop	; 3	
nextline	
	lda #$F9
	sta $d012			
	sta waitbyte
nextirql
	lda #<nextirq
	sta $fffe			
	lda bgline	
	asr #$1f
	tay	
	ldx linefade,y
	ldy #$00
	sty idlebyte
	lda #$00
	sta sprhibyte
	stx $d020
	stx $d021
	inc bgline
	lsr $d019
	inc waitbyte
	inc waitzp
	sta $d015
	lda #$1b		
	inc xpos
	inc ypos	
	ldx bgcolor
	stx $d020
	ldx fgcolor
	stx $d021
	sta $d011
	jsr updatesprpos
	jsr updatescroll	
nextirqh
	lda #>nextirq
	sta $ffff	
	pla
	pla
	pla
	jsr $3015
	bcc *+5
	inc $d800
	rti
	
; Aggiornamento posizione sprite	
; ******************************
updatesprpos	
	sec
	lda spritepos
	sbc spritespd	
	cmp #$11
	bcs sprskip		
	adc #$30
	sta temp
	lda $07fb
	sta $07fa
	lda $07fc
	sta $07fb
	lda $07fd
	sta $07fc
	lda $07fe
	sta $07fd
	lda	$07ff
	sta $07fe	
	ldx nextsprite	
	inx
	cpx #(((spritebase&$3fff)/$40)+6)&$ff
	bne skipfix
	ldx #((spritebase&$3fff)/$40)
skipfix	
	stx nextsprite
	stx $07ff		
	lda  temp
sprskip		
	sta spritepos
	clc
	sta $d004
	adc #$30
	sta $d006
	adc #$30
	sta $d008
	adc #$30
	sta $d00a
	adc #$30
	sta $d00c
	rol sprhibyte
	adc #$30
	sta $d00e
	rol sprhibyte
	ldx sprhibyte
	lda sprhibyte,x	
	sta $d010		
genexit	
	rts

; Aggiorna contenuto scroll
; *************************	
updatescroll
	ldy #$00
	lda spritepos
	and #$0f
	bne	genexit
	lda mirror
	eor #$80
	sta mirror	
	bne settrigger
	inc scrollpos
	bne settrigger
	inc scrollpos+1
settrigger	
	lda (scrollpos),y	
	asl
	ror nextaction	
nohifix	
	lda spritepos
	lsr
	lsr
	lsr
	lsr
	sta temp
	sec
	lda #$04
	sbc temp	
	sta temp1	
	lda (scrollpos),y	
	bne scrollwrite
	lda scrollbeg
	sta scrollpos
	lda scrollbeg+1
	sta scrollpos+1
	lda (scrollpos),y
scrollwrite	
	asl	
	and #$7f
	ora mirror
	asl
	sta temp
	tya
	rol
	asl temp
	rol 
	adc #>charset
	sta chself0+2	
	adc #$04
	sta chself1+2
	ldy temp
	lda #(spritebase/$4000)
	sta temp
	lda nextsprite
	asl 
	rol temp
	asl
	rol temp
	asl
	rol temp
	asl 
	rol temp
	asl
	rol temp
	asl
	rol temp	
	adc temp1
	tax	
	lda temp
	sta sprself0+2
	sta sprself1+2
	lda #$01
	sta temp
chself0	
	lda $ff00,y
sprself0
	sta $ff03,x
chself1
	lda $ff00,y
sprself1
	sta $ff00+$1b,x
	inx
	inx
	inx
	iny
	asl temp
	bcc chself0
exitupdate	
	rts

; Routine di mantainance varie
; ****************************	
	
getirq
	lda #$fe
	ldy #<lower2
	ldx #>lower2
	rts
setcolorfx
	sta bgcolorfx
	stx fgcolorfx
	rts
setcolor
	sta bgcolor
	stx fgcolor
	rts
inittext
	sta scrollbeg
	sta scrollpos
	stx scrollbeg+1
	stx scrollpos+1
	rts
setfx
	cmp #$00
	beq nofx
	lsr
	bcs sfsqr
	ldx #$2c
	ldy #$b9
stafx	
	stx self0
	stx self1	
	sty self01
	sty self11
	rts
sfsqr	
	lsr
	bcs sfspc
	ldy #$2c
	ldx #$bd
	bne stafx
sfspc	
	ldx #$bd	
	ldy #$19
	bne stafx
nofx
	ldx #$2c
	ldy #$2c
	bne stafx
	
setirq
	sta nextline+1
	stx nextirql+1
	sty nextirqh+1
	rts	
	
; Inizializzazione sprites		
; ************************
initall
	lda #$fc
	sta $d001
	sta $d003
	sta $d01b
	lda #$04
	sta $d005	
	sta $d007
	sta $d009
	sta $d00b
	sta $d00d
	sta $d00f	
	lda #$18
	sta $d000
	lda #$28
	sta $d002
	clc
	lda #$30
	sta $d004
	adc #$30
	sta $d006
	adc #$30
	sta $d008
	adc #$30
	sta $d00a
	adc #$30
	sta $d00c
	adc #$30
	sta $d00e
	lda $d020
	sta $d027
	sta $d028
	lda #$01
	sta $d029
	sta $d02A
	sta $d02B
	sta $d02C
	sta $d02D
	sta $d02E	
	lda #$ff	
	sta $d015
	sta $d01d
	lda #$82
	sta $d010
	lda #$03
	sta $d017
	lda #$48
	sta spritepos
	lda #((margsin&$3fff)/$40)
	sta $07f8
	lda #((margdes&$3fff)/$40)
	sta $07f9	
	lda #$00
	sta zeroval		
	ldx #((spritebase&$3fff)/$40)+5	;
	stx nextsprite								; Test only
	stx $07ff	
	dex
	stx	$07fe
	dex
	stx $07fd
	dex
	stx $07fc
	dex
	stx $07fb
	dex
	stx $07fa
	lda #$80
	sta mirror
	lda #$02
	sta spritespd		

; Apply mask of sine/cosine tables	
andsin
	ldx #$00
andlp	
	lda sine,x
	and #$07
	sta sine,x
	lda cosine,x
	and #$07
	sta cosine,x
	dex
	bne andlp
	
; Disabilita il tasto restore	
; ***************************
disrestore
	lda #<nmi
	sta $0318 
	sta $fffa 
	lda #>nmi 
	sta $0319 
	sta $fffb 
	lda #$81 
	sta $dd0d
	lda #$01
	sta $dd04 
	lda #$00 
	sta $dd05 
	lda #%00011001
	sta $dd0e 
	rts 
nmi	
	rti 
	
; Dati vari
; *********
	
spritepos
.byte	$00

sprhibyte
.byte	$00
.byte	$82,$C2	

align $08
blockpointer
.byte	<block,<block+8,<block+16,<block+24,<block+32,<block+40,<block+48,<block+56
align $08
block2pointer
.byte	<block2,<block2+8,<block2+16,<block2+24,<block2+32,<block2+40,<block2+48,<block2+56

align $10
linefade
.byte	$01,$0d,$07,$0f,$03,$05,$0a,$0c
.byte	$0c,$0a,$05,$03,$0f,$07,$0d,$01

block 	
.byte %11111111
.byte %10000000
.byte %10000000
.byte %10000000
.byte %10000000
.byte %10000000
.byte %10000000
.byte %10000000

.byte %11111111
.byte %01000000
.byte %01000000
.byte %01000000
.byte %01000000
.byte %01000000
.byte %01000000
.byte %01000000

.byte %11111111
.byte %00100000
.byte %00100000
.byte %00100000
.byte %00100000
.byte %00100000
.byte %00100000
.byte %00100000

.byte %11111111
.byte %00010000
.byte %00010000
.byte %00010000
.byte %00010000
.byte %00010000
.byte %00010000
.byte %00010000

.byte %11111111
.byte %00001000
.byte %00001000
.byte %00001000
.byte %00001000
.byte %00001000
.byte %00001000
.byte %00001000

.byte %11111111
.byte %00000100
.byte %00000100
.byte %00000100
.byte %00000100
.byte %00000100
.byte %00000100
.byte %00000100

.byte %11111111
.byte %00000010
.byte %00000010
.byte %00000010
.byte %00000010
.byte %00000010
.byte %00000010
.byte %00000010

.byte %11111111
.byte %00000001
.byte %00000001
.byte %00000001
.byte %00000001
.byte %00000001
.byte %00000001
.byte %00000001

block2
.byte $00	; Waste!
.byte %00000000
.byte %00000000
.byte %00000000
.byte %00001000
.byte %00000000
.byte %00000000
.byte %00100000
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %00000100
.byte %00000000
.byte %00000000
.byte %00010000
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %00000010
.byte %00000000
.byte %00000000
.byte %00001000
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %00000001
.byte %00000000
.byte %00000000
.byte %00000100
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %10000000
.byte %00000000
.byte %00000000
.byte %00000010
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %01000000
.byte %00000000
.byte %00000000
.byte %00000001
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %00100000
.byte %00000000
.byte %00000000
.byte %10000000
.byte %00000000

.byte %00000000
.byte %00000000
.byte %00000000
.byte %00010000
.byte %00000000
.byte %00000000
.byte %01000000
.byte %00000000

; Sinus tables
; ************
align $0100
sine
dc.b 25,24,23,23,22,21,21,20,20,19,18,18,17,17,16,15
dc.b 15,14,14,13,13,12,12,11,11,10,10,9,9,8,8,7
dc.b 7,6,6,6,5,5,4,4,4,3,3,3,2,2,2,2
dc.b 1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0
dc.b 0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1
dc.b 1,2,2,2,3,3,3,3,4,4,5,5,5,6,6,7
dc.b 7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,15
dc.b 15,16,16,17,18,18,19,19,20,21,21,22,22,23,24,24
dc.b 25,25,26,27,27,28,28,29,30,30,31,31,32,33,33,34
dc.b 34,35,36,36,37,37,38,38,39,39,40,40,41,41,42,42
dc.b 42,43,43,44,44,44,45,45,46,46,46,46,47,47,47,48
dc.b 48,48,48,48,49,49,49,49,49,49,49,49,49,49,49,49
dc.b 49,49,49,49,49,49,49,49,49,49,49,48,48,48,48,48
dc.b 47,47,47,47,46,46,46,45,45,45,44,44,43,43,43,42
dc.b 42,41,41,40,40,39,39,38,38,37,37,36,36,35,35,34
dc.b 34,33,32,32,31,31,30,29,29,28,28,27,26,26,25,25

align $0100
cosine
dc.b 0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1
dc.b 1,2,2,2,2,3,3,3,4,4,4,5,5,6,6,6
dc.b 7,7,8,8,9,9,10,10,11,11,12,12,13,13,14,14
dc.b 15,16,16,17,17,18,19,19,20,20,21,22,22,23,23,24
dc.b 25,25,26,26,27,28,28,29,30,30,31,31,32,33,33,34
dc.b 34,35,35,36,36,37,38,38,39,39,40,40,41,41,41,42
dc.b 42,43,43,44,44,44,45,45,45,46,46,46,47,47,47,47
dc.b 48,48,48,48,49,49,49,49,49,49,49,49,49,49,49,49
dc.b 49,49,49,49,49,49,49,49,49,49,49,49,48,48,48,48
dc.b 47,47,47,47,46,46,46,45,45,45,44,44,44,43,43,42
dc.b 42,41,41,41,40,40,39,39,38,38,37,36,36,35,35,34
dc.b 34,33,33,32,31,31,30,30,29,28,28,27,26,26,25,25
dc.b 24,23,23,22,22,21,20,20,19,19,18,17,17,16,16,15
dc.b 14,14,13,13,12,12,11,11,10,10,9,9,8,8,7,7
dc.b 6,6,6,5,5,4,4,4,3,3,3,2,2,2,2,1
dc.b 1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0

align $0100
charset
incbin "charset.raw"

; Sprite laterali
; ***************
margsin
.byte	$00,$00,$00,$00,$00,$00,$ff,$ff,$ff
.byte	$ff,$ff,$fe,$ff,$ff,$fc,$ff,$ff,$fc
.byte	$ff,$ff,$f8,$ff,$ff,$f0,$ff,$ff,$f0
.byte	$ff,$ff,$f8,$ff,$ff,$fc,$ff,$ff,$fc
.byte	$ff,$ff,$fe,$ff,$ff,$ff,$ff,$ff,$ff
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
margdes
.byte	$00,$00,$00,$00,$00,$00,$ff,$ff,$ff
.byte	$7f,$ff,$ff,$3f,$ff,$ff,$3f,$ff,$ff
.byte	$1f,$ff,$ff,$0f,$ff,$ff,$0f,$ff,$ff
.byte	$1f,$ff,$ff,$3f,$ff,$ff,$3f,$ff,$ff
.byte	$7f,$ff,$ff,$ff,$ff,$ff,$ff,$ff,$ff
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
; Sprite scroller
; ***************
spritebase
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
nextaction
.byte   $00			; Next action byte
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00,$00,$00,$00,$00,$00,$00,$00,$00
.byte	$00			; Idle byte