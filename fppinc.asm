; FPP zoomer - Freshness 2012 for spires
; **************************************

; Basic settings (Zero page)
; **************************

fx1cnt		equ	$20
fx3cnt		equ	$21
poszh		equ	$22
endcb		equ	$23
chartrigger	equ	$24
temp5		equ	$25
posx		equ	$26
posy		equ	$27
posz		equ	$28
temp3		equ	$29
temp2		equ	$2a
temp		equ	$2b
fgcolor		equ	$2c
bgcolor		equ	$2d
framecol	equ	$2e
; Costants
; ********
; Addresses
base	equ	$8000
; Offsets
sprite		equ $0000	; size $1000
code		equ	$1000	; size $1800
charset		equ	$2800	; + empty sprite and 3 sinus table; size = $0400
scnpage		equ	$2c00	; size $0400
; Various
fstraster	equ	$3c		; First raster (for effect)
based011	equ	$18		; Original value of D011
xsprite		equ	$54		; Start x position of sprites
nextirq		equ	preirq	; Next IRQ handler
storybw		equ $80		; Wait amount for storyboard (in 1/50 sec)


; Macros

mac wait46bg
	lda prntable+{1}
	sty $d017
	sta base+scnpage+$03f9
	sta base+scnpage+$03fa			
	sta base+scnpage+$03fb	
	sta base+scnpage+$03fc
	sta base+scnpage+$03fd
	sta base+scnpage+$03fe
	stx $d017	
	sta base+scnpage+$03ff
	lda #((({1}+fstraster+$01)&7) | based011)
	sta $d011	
endm

; Precalculated sprite tables (Size $1000)
; ****************************************
*=base+sprite
sprite0
ds.b	$40*$20
sprite1
ds.b	$40*$20

; Code entry
; **********
*=base+code

; Service routines jump table
; ***************************
	jmp initscreen		; Prepare screen mask & textures (Y = $d800 color)
	jmp	initio			; Prepare sprite registers
	jmp getirq			; A=raster, X/Y = Hi/lo irq handler
	jmp setirq			; A=raster, X/Y = Hi/lo irq handler
	jmp setcolor		; X=External color, Y=Frame color
	jmp setfxcolor		; X=color bit 0, Y=color bit 1	
	jmp movecoord		; A=00000zyx toggle x,y,z movement
	jmp checkend		; Set carry if part has finished

; Story IRQ (trigger events)
; **************************
storyirq
	pha
	txa
	pha
	tya
	pha		
	jsr setgraph
	lsr $d019
	lda #fstraster-8
	sta $d012
	lda #<preirq
	sta $fffe
	dec storywait
	bne stepon	
	lda #storybw
	sta storywait	
	ldx storypos
	lda fxstory,x
	inc storypos
	ora strx
	sta strx	
stepon
	lda strx
	beq exitfx	
	lsr
	bcc *+5
	jsr fx1
	lsr
	bcc *+5
	jsr fx2	
	lsr
	bcc *+5
	jsr fx3
	lsr
	bcc *+5
	jsr fx4	
	lsr
	bcc *+5
	jsr fx5
	lsr
	bcc *+5
	jsr fx6	
	lsr
	bcc *+5
	jsr fx7
	lsr
	bcc *+5
	jsr fx8		
exitfx	
	pla
	tay
	pla
	tax
	pla
	rti
	
storywait
dc.b 1

; Prepare correct registers value
; *******************************
preirq
	lsr $d019
	lda #fstraster
	sta $d012
	lda #<fstirq
	sta $fffe
	ldx #$ff
	ldy #$00
	pla
	pla
	pla
	lda framecol
	sta $d021	
	sta $d023
	lda #<secirq
	cli
	jmp *
	
; First IRQ
; *********
fstirq  		
	stx $d019			; 6 Ack IRQ	
	inc $d012			; 6 IRQ on next line (+36 Kernal IRQ)		
	sta $fffe			; 4 		
	cli					; 2 IRQ on
	sty base+charset+$40+0	; 4 Restore char definition
	lda #$e0
	sta base+charset+$40+8
	lda #$0b
	sta base+charset+$40+16
	lda prntable		; 4 load first line	
	nop					; 2
	nop					; 2
	nop					; 2
	nop					; 2		
	nop					; 2
	nop					; 2 (62-64) <= IRQ
	nop					; 2 (64-66) <= IRQ 	
	nop
	nop
	nop
	nop
	nop
	nop
	brk

; Synced IRQ
; **********	
secirq	
	stx $d017
	sta base+scnpage+$03f9
	sta base+scnpage+$03fa
	sta base+scnpage+$03fb
	sta base+scnpage+$03fc
	sta base+scnpage+$03fd
	sta	base+scnpage+$03fe
	sta base+scnpage+$03ff
setfgcol	
	lda #$00
	sta $d021
	
; Speedcode
; *********	
	wait46bg 1
	wait46bg 2
	wait46bg 3
	wait46bg 4
	wait46bg 5
	wait46bg 6
	wait46bg 7
	wait46bg 8
	wait46bg 9
	wait46bg 10
	wait46bg 11
	wait46bg 12
	wait46bg 13
	wait46bg 14
	wait46bg 15
	wait46bg 16
	wait46bg 17
	wait46bg 18
	wait46bg 19
	wait46bg 20
	wait46bg 21
	wait46bg 22
	wait46bg 23
	wait46bg 24
	wait46bg 25
	wait46bg 26
	wait46bg 27
	wait46bg 28
	wait46bg 29
	wait46bg 30
	wait46bg 31
	wait46bg 32
	wait46bg 33
	wait46bg 34
	wait46bg 35
	wait46bg 36
	wait46bg 37
	wait46bg 38
	wait46bg 39
	wait46bg 40
	wait46bg 41
	wait46bg 42
	wait46bg 43
	wait46bg 44
	wait46bg 45
	wait46bg 46
	wait46bg 47
	wait46bg 48
	wait46bg 49
	wait46bg 50
	wait46bg 51
	wait46bg 52
	wait46bg 53
	wait46bg 54
	wait46bg 55
	wait46bg 56
	wait46bg 57
	wait46bg 58
	wait46bg 59
	wait46bg 60
	wait46bg 61
	wait46bg 62
	wait46bg 63
	wait46bg 64
	wait46bg 65
	wait46bg 66
	wait46bg 67
	wait46bg 68
	wait46bg 69
	wait46bg 70
	wait46bg 71
	wait46bg 72
	wait46bg 73
	wait46bg 74
	wait46bg 75
	wait46bg 76
	wait46bg 77
	wait46bg 78
	wait46bg 79
	wait46bg 80
	wait46bg 81
	wait46bg 82
	wait46bg 83
	wait46bg 84
	wait46bg 85
	wait46bg 86
	wait46bg 87
	wait46bg 88
	wait46bg 89
	wait46bg 90
	wait46bg 91
	wait46bg 92
	wait46bg 93
	wait46bg 94
	wait46bg 95
	wait46bg 96
	wait46bg 97
	wait46bg 98
	wait46bg 99
	wait46bg 100
	wait46bg 101
	wait46bg 102
	wait46bg 103
	wait46bg 104
	wait46bg 105
	wait46bg 106
	wait46bg 107
	wait46bg 108
	wait46bg 109
	wait46bg 110
	wait46bg 111
	wait46bg 112
	wait46bg 113
	wait46bg 114
	wait46bg 115
	wait46bg 116
	wait46bg 117
	wait46bg 118
	wait46bg 119
	wait46bg 120
	wait46bg 121
	wait46bg 122
	wait46bg 123
	wait46bg 124
	wait46bg 125
	wait46bg 126
	wait46bg 127	
	wait46bg 128
	wait46bg 129		
	wait46bg 130
	; Reset sprite definitions
	lda #<(empty/$40)
	sta base+scnpage+$03f9
	sta base+scnpage+$03fa
	sta base+scnpage+$03fb
	sta base+scnpage+$03fc
	sta base+scnpage+$03fd
	sta base+scnpage+$03fe
	sta base+scnpage+$03ff
	; Interrupt Stuff (Prepare for next IRQ)
	lda framecol
	sta $d021		
	lsr $d019	
	nop
	nop
	nop
	lda #$00
	sta $d017
nxtrstr
	lda #fstraster-8
	sta $d012
nxtirql
	lda #<preirq
	sta $fffe
nxtirqh
	lda #>preirq
	sta $ffff
	pla
	pla
	pla		
	; Reset colors
	lda fgcolor	
	sta setfgcol+1
	lda #$ff
	sta base+charset+$40+0
	sta base+charset+$40+8
	sta base+charset+$40+16	
	pha
	pla
	pha
	pla	
	nop
	cli
	lda bgcolor	
	sta $d023
	sta $d021
	
	; Precalc FX	
	jsr calcfx
	lda chartrigger
	bne *+3
	rti
	; Special screen mode FX
	lda base+charset+$40+4
	bne *+4
	lda #$03
	cmp #$80
	rol
	cmp #$80
	rol	
	sta base+charset+$40+4
	pha
	and #$0f
	sta temp2
	pla
	and #$f0
	sta temp
	lda base+charset+$40+4+8
	and #$f0
	ora temp2
	sta base+charset+$40+4+8
	lda base+charset+$40+4+16
	and #$0f
	ora temp
	sta base+charset+$40+4+16	
	rti

; Update precalculated FX 
; ***********************

calcfx
	clc			; Update z coordinate index
	lda posz	;
movez		
	adc #$00
	sta posz
	tay	
	rol
	and #$01
	eor poszh
	sta poszh
	lsr
	lda sinusz1,y
	bcc *+5
	lda sinusz2,y	
	pha				; A=X => z value	
	sta temp	
	sta updxval	; Used on main loop
	and #$1f			
	clc
	adc #<(sprite0/$40)	; Low bits ($1f) of sprite
	sta temp2
movex
	bit posx
	ldy posx
	lda sinusx,y
	jsr divdim	
	sta temp3		; <= ycoord mod zsize
movey
	bit posy		; Update y coordinate index
	ldy posy		;
	lda sinusy,y
	jsr divdim		; update temp2 (eor)
	tax				; <= ycoord mod zsize

	lda temp2	
	ldy #$83
updloop	
	dex
	bne *+6
updxval=*+1	
	ldx #$00
	eor #$20
	sta prntable-1,y
	dey
	bne updloop	
	
	; Update on a $20 basis	
	pla
	asl
	rol
	rol
	and #$01
	rol	
	tay
	ldx yspritepos,y	
	stx $d003
	dex
	stx $d005
	dex
	stx $d007
	dex
	stx $d009
	dex
	stx $d00b
	dex
	stx $d00d
	dex
	stx $d00f
	lda temp3
	adc #xsprite	; Carry already cleared
	sta $d002
	adc #$18
	sta $d004
	adc #$18
	sta $d006
	adc #$18
	sta $d008
	adc #$18
	sta $d00a
	adc #$18
	sta $d00c
	adc #$18
	sta $d00e
	lda #$00
	ror
	sta $d010
	rts	

; Compute A/(temp3)
; *****************
; temp2 <= eored A/(temp3) times
; A     <= A mod (temp3)
divdim
	cmp temp
	bcs divtst
divexit
	rts
divtst
	beq divexit 
	sbc temp
	tax
	lda temp2
	eor #$20
	sta temp2
	txa
	bcs divdim
	
yspritepos
.byte	fstraster+1,fstraster+1-7,fstraster+1-14,fstraster+21	; Last byte unused

; Initialize I/O
; **************
initio	
	lda #$fe
	sta $d015
	ldx #fstraster+1
	stx $d003
	dex
	stx $d005
	dex
	stx $d007
	dex
	stx $d009
	dex
	stx $d00b
	dex
	stx $d00d
	dex
	stx $d00f
	clc
	lda #xsprite
	sta $d002
	adc #$18
	sta $d004
	adc #$18
	sta $d006
	adc #$18
	sta $d008
	adc #$18
	sta $d00a
	adc #$18
	sta $d00c
	adc #$18
	sta $d00e
	lda #$00
	sta $d017
	lda #$ff
	sta $d01b
	lda #<((empty&$3fff)/$40)
	base+scnpage+$03f8
	base+scnpage+$03f9
	base+scnpage+$03fa
	base+scnpage+$03fb
	base+scnpage+$03fc
	base+scnpage+$03fd
	base+scnpage+$03fe	
	lda #$00
	sta posx
	sta posy
	sta posz
	sta poszh
	sta fx1cnt
	sta fx3cnt
	sta chartrigger
	sta endcb	
	rts

	
; Screen masking (unoptimized)
; ****************************
initscreen	
	ldx #$27	
isloop1		
	lda #$0b
	sta base+scnpage,x	
	lda #$08
	sta base+scnpage+$0028,x
	tya
	sta $d800+$0000,x
	sta $d800+$0028,x	
	dex 
	bmi isexit1 
	jmp isloop1
isexit1	
	ldx #$17
	lda #$09
isloop2	
	cpx #$00
	beq *+5
	sta base+scnpage+$001c,x
	sta base+scnpage+$0044,x	
	pha
	lda #$f0
	sta base+scnpage+$0320,x
	pla
	lda #$0b
	cpx #$01
	bne *+4
	lda #$0A
	dex 
	bpl isloop2
	
; Precalc zoomed sprite definitions
; *********************************
preparesprite
	lda #$01
	sta sqrloopxxt+1
	lda #$21
	sta sqrcmp+1
	lda #$03
	sta temp2
	lda #<sprite0
	sta sqrself1+1
	sta savepos
	lda #>sprite0
	sta sqrself1+2
	sta savepos+1
	lda #<sprite1
	sta sqrself2+1
	sta savepos+2
	lda #>sprite1
	sta sqrself2+2	
	sta savepos+3
sqrloopxxt	
	ldy #$01
sqrloopxt	
	sty temp
	lda #$18
	sta sqrcarry	
	ldx #$00
sqrloopmd	
	lda #$01
sqrcarry	
	sec
	rol
	dec temp
	bne sqrskpeor
	pha
	lda sqrcarry
	eor #$20
	sta sqrcarry
	sty temp
	pla
sqrskpeor	
	bcc sqrcarry
sqrself1
	sta sprite0,x
	eor #$ff
sqrself2	
	sta sprite1,x
	inx
	cpx #$15
	bne sqrloopmd
	clc
	lda sqrself1+1
	adc #$40
	sta sqrself1+1
	lda sqrself1+2
	adc #$00
	sta sqrself1+2
	clc
	lda sqrself2+1
	adc #$40
	sta sqrself2+1
	lda sqrself2+2
	adc #$00
	sta sqrself2+2	
	iny
sqrcmp	
	cpy #$21
	bne sqrloopxt
	clc
	lda savepos
	adc #$15
	sta sqrself1+1
	sta savepos
	lda savepos+1
	sta sqrself1+2
	clc
	lda savepos+2
	adc #$15
	sta sqrself2+1
	sta savepos+2
	lda savepos+3
	sta sqrself2+2
	clc
	lda sqrloopxxt+1
	adc #$20
	sta sqrloopxxt+1
	clc
	lda sqrcmp+1
	adc #$20
	sta sqrcmp+1	
	dec temp2
	beq sqrexit
	jmp sqrloopxxt
sqrexit
	rts

savepos	
.byte	$00,$00,$00,$00
	
; FX routines	
; ***********	
fx1
	pha
	ldy fx1cnt
	lda fade,y
	tay
	ldx #$00
	jsr base+code+12
	ldy fx1cnt
	iny
	cpy #$10
	bne *+10	
	lda strx
	and #$fe
	sta strx
	sty fx1cnt
	pla
	rts
fx2
	pha
	ldy fx1cnt
	lda fade,y
	tay
	ldx #$00
	jsr base+code+12
	ldy fx1cnt
	dey
	bpl *+10	
	lda strx
	and #$fd
	sta strx
	sty fx1cnt
	pla
	rts
fx3
	pha
	ldy fx3cnt
	ldx fadesq11,y
	lda fadesq12,y
	tay	
	jsr base+code+15
	ldy	fx3cnt	
	iny
	cpy #$10
	bne *+10
	lda strx
	and #$fb
	sta strx
	sty fx3cnt
	pla
	rts
fx4
	pha
	ldy fx3cnt
	ldx fadesq11,y
	lda fadesq12,y
	tay	
	jsr base+code+15
	ldy	fx3cnt	
	dey	
	bpl *+10
	lda strx
	and #$f7
	sta strx
	sty fx3cnt
	pla
	rts
fx5
	pha
	lda #$04
	jsr code+base+18
	lda strx
	and #$ef
	sta strx
	pla
	rts
fx6
	pha
	lda #$02
	jsr code+base+18
	lda strx
	and #$df
	sta strx	
	pla
	rts
fx7
	pha
	lda #$01
	jsr code+base+18
	lda strx
	and #$bf
	sta strx		
	pla
	rts
fx8	
	lda #$01
	sta endcb	
	lsr
	sta storypos
	lda strx
	and #$7f
	sta strx
	rts	

; Service routines definitions
; ****************************

movecoord
	sta temp5
	lsr temp5	
	bcc *+10
	lda movex
	eor #$c2
	sta movex
	lsr temp5	
	bcc *+10
	lda movey
	eor #$c2
	sta movey
	lsr temp5	
	bcc *+10
	lda movez+1
	eor #$01
	sta movez+1
	rts

setcolor
	stx	bgcolor
	sty	framecol	
	rts
	
setfxcolor
	stx fgcolor	
	sty $d028
	sty $d029
	sty $d02a
	sty $d02b
	sty $d02c
	sty $d02d
	sty $d02e
	rts
	
getirq
	lda #fstraster-16
	ldx #>storyirq
	ldy #<storyirq
	rts
	
setirq
	sta nxtrstr+1
	stx nxtirqh+1
	sty nxtirql+1
	rts
	
setgraph
	lda #(((charset&$3fff)/$0800)*2)+(scnpage/$0400)*16; Restore correct VIC pointers
	sta $d018	
	lda $dd00
	and #$fc
	ora #(3^(base/$4000))	
	sta $dd00	
checkend
	lda endcb
	lsr
	rts
		
storypos
dc.b 0
strx
dc.b 0

; Fades
; *****
fade	
.byte	$00,$06,$09,$02,$0B,$04,$08,$0C,$0E,$05,$0A,$03,$0F,$07,$0D,$0F
fadesq11
.byte	$00,$00,$06,$09,$02,$0B,$04,$08,$0C,$0E,$05,$0A,$03,$0F,$07,$01
fadesq12
.byte	$00,$06,$06,$09,$09,$02,$02,$0B,$0B,$04,$04,$08,$08,$0C,$0E,$0C

; 67 byte hole
; ************

align $0100
sinusz2
dc.b 73,75,76,77,79,80,81,82,83,84,85,86,86,87,87,88
dc.b 88,88,88,88,88,88,88,87,87,86,85,85,84,83,82,80
dc.b 79,78,76,75,73,71,69,67,66,64,62,59,57,55,53,51
dc.b 49,46,44,42,40,38,36,33,31,29,27,25,24,22,20,18
dc.b 17,15,14,13,11,10,9,8,8,7,6,6,6,5,5,5
dc.b 5,6,6,7,7,8,9,9,10,12,13,14,15,17,18,20
dc.b 21,23,25,27,28,30,32,34,36,38,40,42,44,46,48,49
dc.b 51,53,55,57,58,60,62,63,65,66,67,68,70,71,72,73
dc.b 73,74,75,75,76,76,76,77,77,77,77,77,76,76,76,75
dc.b 75,74,74,73,72,72,71,70,69,68,67,66,65,64,63,62
dc.b 62,61,60,59,58,57,56,55,54,54,53,52,52,51,51,50
dc.b 50,49,49,49,49,48,48,48,48,48,49,49,49,49,50,50
dc.b 50,51,51,52,52,53,53,54,55,55,56,56,57,58,58,59
dc.b 59,60,60,61,61,62,62,62,62,63,63,63,63,63,63,62
dc.b 62,62,61,61,60,60,59,58,57,57,56,55,54,52,51,50
dc.b 49,47,46,45,43,42,40,39,37,36,35,33,32,30,29,27


; CHARSET (only four chars used!)
; *******************************
; Empty sprite (For masking upper/lower area of fx)
; *************************************************
*=base+charset
empty
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00
.byte $00,$00,$00,$00,$00,$00,$00,$00,$00

; Spare byte
; **********
.byte $00

; Charset (only four chars defined)
; *********************************
.byte	%00000000
.byte	%00000000
.byte	%00000000
.byte	%00000000
.byte	%00000000
.byte	%00000000
.byte	%00000000
.byte	%00000000

.byte	%11100000
.byte	%11100000
.byte	%11100000
.byte	%11100000
.byte	%11100000
.byte	%11100000
.byte	%11100000
.byte	%11100000

.byte	%00001011
.byte	%00001011
.byte	%00001011
.byte	%00001011
.byte	%00001011
.byte	%00001011
.byte	%00001011
.byte	%00001011

.byte	%11111111
.byte	%11111111
.byte	%11111111
.byte	%11111111
.byte	%11111111
.byte	%11111111
.byte	%11111111
.byte	%11111111

; Story: (Max 28 byte)
; bit 0-7:
; 0 - frame fade in
; 1 - frame fade out
; 2 - chess fade in
; 3 - chess fade out
; 4 - trigger z movement
; 5 - trigger y movement
; 6 - trigger x movement
; 7 - end
fxstory
.byte	$01,$44,$00,$00,$20,$00,$00,$10,$00,$00,$00,$00,$00,$0A,$F0,$00,$00,$00,$00,
.byte $00	; Actual story (Ends at $F0 - bit 7 set)
; fx available steps
.byte	$00,$00,$00,$00,$00,$00,$00,$00

prntable	; 130 byte table relocatable; 

; Sine table (For each coordinate)
; ********************************
align $0100
sinusz1
dc.b 27,26,25,23,22,21,20,19,17,17,16,15,14,14,13,13
dc.b 12,12,12,12,12,12,12,12,13,14,14,15,16,17,18,19
dc.b 20,22,23,25,26,28,30,32,33,35,37,39,42,44,46,48
dc.b 50,52,55,57,59,61,63,65,68,70,72,74,75,77,79,81
dc.b 82,84,85,87,88,89,90,91,92,93,93,94,94,94,95,95
dc.b 95,94,94,94,93,93,92,91,90,89,88,87,85,84,83,81
dc.b 79,78,76,74,72,71,69,67,65,63,61,59,57,55,53,51
dc.b 50,48,46,44,42,41,39,38,36,35,33,32,31,30,29,28
dc.b 27,26,26,25,24,24,24,23,23,23,23,23,23,24,24,24
dc.b 25,25,26,27,27,28,29,30,30,31,32,33,34,35,36,37
dc.b 38,39,40,41,42,43,44,44,45,46,47,47,48,49,49,50
dc.b 50,51,51,51,51,52,52,52,52,52,52,51,51,51,51,50
dc.b 50,49,49,48,48,47,47,46,46,45,44,44,43,43,42,41
dc.b 41,40,40,39,39,39,38,38,38,38,37,37,37,37,37,38
dc.b 38,38,38,39,39,40,41,41,42,43,44,45,46,47,48,49
dc.b 51,52,53,55,56,57,59,60,62,63,65,66,68,69,71,72
sinusy
dc.b 128,121,115,109,103,97,90,85,79,73,67,62,57,52,47,42
dc.b 37,33,29,25,22,18,15,12,10,8,6,4,3,2,1,1
dc.b 1,1,1,2,3,5,6,8,11,13,16,19,23,26,30,34
dc.b 39,43,48,53,58,63,69,74,80,86,92,98,104,110,117,123
dc.b 129,135,142,148,154,160,166,172,178,183,189,194,200,205,210,214
dc.b 219,223,227,231,234,237,240,243,246,248,250,251,252,253,254,254
dc.b 254,254,254,253,252,250,248,246,244,241,238,235,232,228,224,220
dc.b 215,211,206,201,196,190,185,179,173,167,162,155,149,143,137,131
dc.b 124,118,112,106,100,93,88,82,76,70,65,59,54,49,44,40
dc.b 35,31,27,23,20,17,14,11,9,7,5,3,2,1,1,1
dc.b 1,1,2,3,4,5,7,9,12,15,18,21,24,28,32,36
dc.b 41,45,50,55,61,66,72,77,83,89,95,101,107,113,120,126
dc.b 132,138,145,151,157,163,169,175,181,186,192,197,202,207,212,216
dc.b 221,225,229,232,236,239,242,244,247,249,250,252,253,254,254,254
dc.b 254,254,253,252,251,249,247,245,243,240,237,233,230,226,222,218
dc.b 213,208,203,198,193,188,182,176,170,165,158,152,146,140,134,128
sinusx
dc.b 1,1,1,1,1,2,2,3,3,4,5,5,6,7,8,9
dc.b 10,11,12,13,15,16,17,18,19,20,21,22,23,24,25,26
dc.b 27,28,29,29,30,30,31,31,31,31,31,31,31,31,31,31
dc.b 30,30,29,29,28,27,26,25,24,23,22,21,20,19,18,17
dc.b 16,15,13,12,11,10,9,8,7,6,5,5,4,3,3,2
dc.b 2,1,1,1,1,1,1,1,1,1,2,2,3,3,4,5
dc.b 5,6,7,8,9,10,11,12,13,15,16,17,18,19,20,21
dc.b 22,23,24,25,26,27,28,29,29,30,30,31,31,31,31,31
dc.b 31,31,31,31,31,30,30,29,29,28,27,26,25,24,23,22
dc.b 21,20,19,18,17,16,15,13,12,11,10,9,8,7,6,5
dc.b 5,4,3,3,2,2,1,1,1,1,1,1,1,1,1,2
dc.b 2,3,3,4,5,5,6,7,8,9,10,11,12,13,15,16
dc.b 17,18,19,20,21,22,23,24,25,26,27,28,29,29,30,30
dc.b 31,31,31,31,31,31,31,31,31,31,30,30,29,29,28,27
dc.b 26,25,24,23,22,21,20,19,18,17,16,15,13,12,11,10
dc.b 9,8,7,6,5,5,4,3,3,2,2,1,1,1,1,1
