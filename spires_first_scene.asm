//;----------------------------------------------------------
//;----------------------------------------------------------
//;				Spires Elder0010 / Freshness - Onslaught
//;----------------------------------------------------------
//;----------------------------------------------------------

	
fade_irq:
		pha txa pha tya pha 

		asl $d019
		
		lda first_irq_state
		cmp #0
		bne wait_fall
		
		ldx fade_pt
		lda fade_table,x
		
		ldx #0
!:
		
		sta $d800,x
		sta $d800+$100,x
		sta $d800+$200,x
		sta $d800+$300,x
		
		dex
		bne !-
		
		
		inc fade_pt
		lda fade_pt
		cmp #18
		bne !+
		
		inc first_irq_state
		jmp endxv
wait_fall:
		
		inc first_irq_delay
		lda first_irq_delay
		cmp #$c0
		bne !+
		

		
		lda #<intro_irq sta $fffe
		lda #>intro_irq sta $ffff
!:
endxv:
		jsr $1003
		
		pla tay pla tax pla
		
		rti
		
		
intro_irq:
		pha txa pha tya pha 

		asl $d019	
		
		lda direzione
		cmp #0 //;GIU
		bne SU
		//;Sta andando GIU
GIU:
		lda $d001 //;ha toccato il bordo?
		cmp #$e5
		bcc !+
		
		inc direzione
		inc crashed
		
	//;	lda #0
	//;	sta sprite_speed
		
!:
		lda $d001
		adc sprite_speed
		sta $d001
		
		//;Ogni 4 frames accelero o decelero..
		inc delay
		lda delay
		cmp #2
		bne !+
		lda #0
		sta delay
			
		inc sprite_speed
!:
		
		jmp endx
SU:
		
		lda rimbalzi
		cmp $d001 //;Ha toccato il bordo superiore di rimbalzo?
		bcc !+
		
		lda rimbalzi
		adc #13
		sta rimbalzi
		
		dec direzione
		lda #0
		sta sprite_speed
!:
			//;Ogni 4 frames accelero o decelero..
		inc delay
		lda delay
		cmp #2
		bne !+
		lda #0
		sta delay
			
		dec sprite_speed
!:

		lda $d001
		sbc sprite_speed
		sta $d001
		
		
		
endx:
		
		jsr move_other
		
		lda crashed
		cmp #0
		beq !+
		jsr scatter_x
	
!:
		//;Mega fix per far sparire gli sprites dopo un tot di rimbalzi
		lda crashed
		cmp #12
		bmi !+
		
		inc tick_timer
		lda tick_timer
		cmp #20
		bne !+
		lda #0
		sta $d015	
		
	
		
		//;Cambio scena
		lda #1
		sta SCENE_STATE
		
		//;punto il fade irq
		lda #<after_intro_irq sta $fffe
		lda #>after_intro_irq sta $ffff
	
		lda #$9b
		sta $d011
		lda #$28
		sta $d012
		
	
!:
		
		jsr $1003
		
		pla tay pla tax pla
		
		rti

		//;muove gli altri sprites appresso al primo
move_other:
		lda $d001
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		sta $d00b
		sta $d00d
		sta $d00f
		rts
		
scatter_x:
	

	
		//;verifico tutti i d010
		ldx #0
		ldy #0

VERIFY:	
		lda sprite_zone,x
		cmp #0
		bne A_DESTRA
A_SINISTRA:
		
		
		lda sprite_direction,x
		cmp #0
		bne SX_AVANTI
SX_INDIETRO:
		
		clc
		
		lda $d000,y
		sbc sprite_speeds,x
		sta $d000,y
		
		//;$14
	
		
		lda $d000,y
		cmp #20
		bcs !+

		lda #1
		sta sprite_direction,x
!:
		jmp endsprmv
		
SX_AVANTI:
		
		clc
		

		lda $d000,y
		adc sprite_speeds,x
		sta $d000,y
		
		bcc !+
		
		lda #0
		sta $d000,y
		
		lda $d010
		ora d010_spr,x
		sta $d010
		lda sprite_zone,x
		adc #1
		sta sprite_zone,x
!:

		jmp endsprmv
//;------------------------
A_DESTRA:
		
		lda sprite_direction,x
		cmp #0
		bne DX_AVANTI
DX_INDIETRO:
	
		lda $d000,y
		sbc sprite_speeds,x
		sta $d000,y
		
		bcs endsprmv
	
		lda $d010
		and d010_spr_off,x
		sta $d010
		
		
		lda #0
		sta sprite_direction,x
		sta sprite_zone,x
	
		jmp endsprmv
		
DX_AVANTI:
		
		lda $d000,y
		adc sprite_speeds,x
		sta $d000,y
		
		cmp #64
		bmi endsprmv
		
		lda sprite_direction,x
		sbc #1
		sta sprite_direction,x
		
endsprmv:	
		iny
		iny
		
		inx
		cpx #8
		beq exlop
		jmp VERIFY
exlop:
		rts
		

rimbalzi:
		.byte $70
		
rimb_pt:
		.byte $0
		
direzione:
		.byte $0

sprite_direction:
		.byte $1,$0,$1,$0,$1,$0,$1,$0
	
sprite_zone:
		.byte $0,$0,$0,$0,$0,$0,$0,$0

sprite_speeds:
		.byte $2,$1,$2,$1,$2,$1,$1,$1

d010_spr:
		.byte %00000001
		.byte %00000010
		.byte %00000100
		.byte %00001000
		.byte %00010000
		.byte %00100000
		.byte %01000000
		.byte %10000000
		
d010_spr_off:
		.byte %11111110
		.byte %11111101
		.byte %11111011
		.byte %11110111
		.byte %11101111
		.byte %11011111
		.byte %10111111
		.byte %01111111
		
	
crashed:
	.byte $0

tick_timer:
	.byte $0
	
//;----------------------------------------------------------
//;----------------------------------------------------------
//;				After intro irq per fade!
//;----------------------------------------------------------
//;----------------------------------------------------------
after_intro_irq:
		pha txa pha tya pha 

		asl $d019	
		
		ldx fade_intro_pt
		lda fade_table_intro,x
		sta $d020
		
		lda fade_intro_d021_pt,x
		sta $d021
		
		cpx #23
		bne !+
		
		dex
		stx fade_intro_pt
		stx fade_intro_d021_pt
		
		inc delayer
		
		
		//;QUI � IL RITARDO DA DIMINUIRE
		lda $1783
		cmp #$81
		bne !+
		
		lda #$fe
		sta delayer
		
		inc delayer_2
		

		
		lda #$1
		sta $d022
		
		lda #$6
		sta $d023
		
		lda #$1b
		sta $d011
	
//;	sta zp2
		lda #$2e
		sta $d012
		
		
		lda #<top_credits_irq sta $fffe
		lda #>top_credits_irq sta $ffff

		
		inc SCENE_STATE
		
		//;lda #$da
		lda #BACKUP_D016
		sta $d016
		
		lda #$1a
		sta $d018
		
		lda #0
		sta delayer
		sta fade_intro_pt
		
!:
		inc fade_intro_pt
		inc fade_intro_d021_pt
		
		jsr $1003
	
		pla tay pla tax pla
		
		rti

	
//;----------------------------------------------------------
//;----------------------------------------------------------
//;		Disegno il logo temporaneo!
//;----------------------------------------------------------
//;----------------------------------------------------------
//;Questo visualizza elder e freshness
top_credits_irq:
		pha txa pha tya pha 

		asl $d019
		
		lda #$1c
		sta $d018
		
		lda #$c8
		sta $d016
	
		
	//	lda #RED
	//;	sta $d020
		
		lda #$78
		sta $d012
		
		lda #$1b
		sta $d011
		
		lda #<mid_credits_irq 
		sta $fffe
		lda #>mid_credits_irq 
		sta $ffff
	
		pla tay pla tax pla
		
		rti	

//;Questo visualizza il logo		
mid_credits_irq:
		pha txa pha tya pha 

		asl $d019
		
		lda scroll_verticale
		sta $d011
		
		lda #$1a
		sta $d018
		
		//;lda #$da
		lda #BACKUP_D016
		sta $d016
		
	//	lda #BLUE
	//	sta $d020
		
		lda #$af
		sta $d012
	
		lda #<bottom_credits_irq sta $fffe
		lda #>bottom_credits_irq sta $ffff

		pla tay pla tax pla
		
		rti	
		

//;Questo visualizza almightygod e psycho	
//;e scrolla
bottom_credits_irq:
		pha txa pha tya pha 
		asl $d019
		
		lda #$1c
		sta $d018
		
		lda #$c8
		sta $d016
		
//		lda #YELLOW
//		sta $d020
	
		lda #$1b
		sta $d011
	
		lda #$f0
		sta $d012
		
FADE_OR_STOP_LO:	
		lda #<logo_fade_irq 
		sta $fffe
FADE_OR_STOP_HI:	
		lda #>logo_fade_irq 
		sta $ffff
		
		
		pla tay pla tax pla
		
		rti	

		
logo_fade_irq:
		pha txa pha tya pha 

		asl $d019	
		
	//	lda #GREEN
	//	sta $d020
	
				clc
		lda target_pt
		cmp #4
		bne NOGO
		lda #$a0
		sta SCRITTE_DELAY+1
NOGO:

		
		//;Devo far scrollare il logo
		//;Fade sui colori del logo
		lda SCENE_STATE
		cmp #3
		bne !+
		
FADE_COL:		
		lda fade_table_logo
		sta $d022
		cmp #$1
		beq !+
		inc FADE_COL+1
		
		jmp MUSIX
!:

SKIP_ME:
		jmp OKGO
OKGO:
		//;Fade sulle scritte dei creditz
		lda can_fade_scritte
		cmp #1
		beq NO_FADE_SCRITTE
		inc delay_fade_scritte
		lda delay_fade_scritte
		//;cmp #$80
SCRITTE_DELAY:
		cmp #$b0
		bne !+
		lda #0
		sta delay_fade_scritte
		inc can_fade_scritte
!:		
		jmp MUSIX
		
NO_FADE_SCRITTE:
		ldy #$0
		ldx fade_pointer_scritte	
!:	
		lda fade_table_logo,x
TARGET_1:
		sta $d800,y
TARGET_2:
		sta $d828,y
		iny
		cpy #70
		bne !-
		
		inc fade_pointer_scritte
		
	
		txa
		cmp #20
		bne !+
		
		//;Cambio scritta
		ldx target_pt
		lda colori_target_lo,x
		sta TARGET_1+1
		adc #$28
		sta TARGET_2+1
		
		lda colori_target_hi,x
		sta TARGET_1+2
		sta TARGET_2+2
		
		inc target_pt
		
		lda #0
		sta fade_pointer_scritte
		sta can_fade_scritte
		
		txa
		cmp #4
		bne !+
		inc SCENE_STATE
		

		
		lda #>SCROLL_CREDITS
		sta SKIP_ME+2
		lda #<SCROLL_CREDITS
		sta SKIP_ME+1
		
		lda #40
		sta fade_pointer_scritte
		
!:
jmp MUSIX
//;----------------------------------------------------------
//;		Faccio rullare via i credits!
//;----------------------------------------------------------
SCROLL_CREDITS:
		
	//	lda #0
	//	sta can_fade_scritte
		inc delay_fade_scritte
		
		lda delay_fade_scritte
		cmp #$54
	//;	cmp #$30
		bne !+
		lda #0
	//;	inc $d020
		sta delay_fade_scritte
		lda #1
		sta can_fade_scritte
!:		
		lda can_fade_scritte
		cmp #1
		bne MUSIX
		
		ldx fade_pointer_scritte
		lda fade_table_logo,x	
!:	
		sta $d800,y
		sta $d828,y
		sta $d883,y
		sta $d883+$28,y
		sta $daa0,y
		sta $daa0+$28,y
		sta $db52,y
		sta $db52+$28,y
		sta $db15,y
		sta $db15+$28,y
		
		iny
		cpy #70
		bne !-
		
		dec fade_pointer_scritte
		cmp #0
		bne MUSIX
		
		lda #<stop_fade_irq 
		sta $fffe
		lda #>stop_fade_irq 
		sta $ffff
		
		lda #0
		sta delay_fade_scritte
		sta can_fade_scritte
		
		inc SCENE_STATE
		
		lda #$1a
		sta $d018
		
		//;lda #$da
		lda #BACKUP_D016
		sta $d016
		
		jsr $1003
		
		jmp endvia
		
MUSIX:	
		
		jsr $1003
		
		lda #$2e
		sta $d012
		
		
		lda #<top_credits_irq 
		sta $fffe
		lda #>top_credits_irq 
		sta $ffff
endvia:		

		pla tay pla tax pla
		
		rti	

//;----------------------------------------------------------
//;----------------------------------------------------------
//;		Qui ho finito di fadare i credits e il logo.  
//;		Faccio scrollare giu il logo
//;----------------------------------------------------------
//;----------------------------------------------------------
stop_fade_irq:
		pha txa pha tya pha 

		asl $d019		
	
		inc delay_fade_scritte
		
		lda delay_fade_scritte
		//;cmp #$60
		cmp #$1
		bne !+
		lda #0
		sta delay_fade_scritte
		lda #1
		sta can_fade_scritte
!:		

		lda can_fade_scritte
		cmp #1
		beq !+
		jmp NOYSCROLL
!:

		clc
		
		lda scroll_verticale		
		adc #1		//; sommo 1
		and #$07	//; mi tengo nel range 0-7
		ora #$18	//; e sistemo gli altri flag
		sta $d011
		sta scroll_verticale
		
		and #$07	//; torno in range 0-7
		beq CANYSCROLL
FINETESTY:
		cmp #$07
		beq TESTYSCROLL
!:
		jmp NOYSCROLL
TESTYSCROLL:		
		lda offset_y
		cmp #9
		bne !-
		jmp AWAY
CANYSCROLL:
		pha
		ldy #$c8
		//;Devo copiare tutte le righe!
!:		
SCRSLFMOD:
		lda $0400+40*10-1,y
		sta $0400+40*11-1,y
		
			
		dey			
		bne !-
		
		clc
		lda SCRSLFMOD+1
		adc #$28
		sta SCRSLFMOD+1
		bcc !+
		inc SCRSLFMOD+2
!:		
		clc
		lda SCRSLFMOD+1+3
		adc #$28
		sta SCRSLFMOD+1+3
		bcc !+
		inc SCRSLFMOD+2+3	
!:		

		
		inc offset_y	
		pla
		//;jmp NOYSCROLL
		jmp FINETESTY
AWAY:

		
		lda #<pre_checker_irq 
		sta $fffe
		lda #>pre_checker_irq 
		sta $ffff

		
NOYSCROLL:
		
		jsr $1003
		
		pla tay pla tax pla
		
		rti	
		

pre_checker_irq:
		pha txa pha tya pha 		
		
		asl $d019	
		
		lda #$5
		sta SCENE_STATE
		
		lda CAN_START_CHECKER
		cmp #1
		bne !+
		
				
		jsr $9006
		stx icirqh1+1
		sty icirql1+1
		sta icrst1+1
		
		stx icirqh2+1
		sty icirql2+1
		sta icrst2+1

		
		lda	#$d4

		ldx	#>stop_scroll_irq
		ldy #<stop_scroll_irq
		jsr $9009
		
		//;Punto gli sprites vuoti
		//;Per mascherare il pixel rotto
		lda #$a9
		sta $0400+$3f8
		sta $0400+$3f9
		sta $0400+$3fa
		sta $0400+$3fb
		sta $0400+$3fc
		sta $0400+$3fd
		sta $0400+$3fe
		sta $0400+$3ff
		
icirql1:
		lda #$00
		sta $fffe
icirqh1:
		lda #$00
		sta $ffff
icrst1:		
		lda #$02
		sta $d012

D011_BACK2:		
		lda #$18	
		sta $d011

		
!:
	//	inc $d020
		
		jsr $1003
		
	//	dec $d020

	
		pla tay pla tax pla
		
		rti	
//;-------------------------------------
//;--QUESTI IRQ SERVONO SOTTO ALLO ZOOM 
//;-------------------------------------
stop_scroll_irq:
		pha txa pha tya pha 

		asl $d019		
	
		//;Visualizzo il logo normale
		
		lda $DD00
		and #%11111100
		ora #%00000011
		sta $DD00

		lda #$1a
		sta $d018
D016_BACK:		
		//;lda #$da
		lda #BACKUP_D016
		sta $d016
	
		
		lda #$02
		sta $d012

		//;lda #$18
	//;	lda #$1c
	//;	sta $d011
		lda #$1f
		sta $d011
		
		//;Verifico se la story � finita
		clc
		jsr $9000+21
		bcc STORY_NORMAL
	

		lda #$ff
		sta $d012
		lda #<scroll_back_top_irq 
		sta $fffe
		lda #>scroll_back_top_irq 
		sta $ffff
		
	//;	lda #$19
	//;	sta $d011
	
		lda #$1f
		sta $d011
	
		sta scroll_verticale
		
		lda #0 
		sta delayer //;RICICLO VARIABILE TEMPORANEA :D per contare quante righe
					//;Deve salire il logo
		jmp GO_AWAY
		
STORY_NORMAL:

		lda #<music_logo_irq 
		sta $fffe
		lda #>music_logo_irq 
		sta $ffff
		

GO_AWAY:	
		pla tay pla tax pla
		
		rti	

music_logo_irq:
		pha txa pha tya pha 

		asl $d019		
		
	//;	lda #$d7
	//;	lda #$e0
	//;	sta $d012
		
icirql2:
		lda #$00
		sta $fffe
icirqh2:
		lda #$00
		sta $ffff
icrst2:		
		lda #$02
		sta $d012

		jsr $1003

	//;	lda #$d8
	//;	lda #BACKUP_D016
		lda #$d8
		sta $d016
		
		lda #$1b
		sta $d011
		
		
		pla tay pla tax pla
		
		rti	
		.pc = * "SNODO pre tech"
//;-------------------------------------
//;-------------------------------------



//;---------------------------------------
//;--SCROLLO SU DI NUOVO TERMINATO LO ZOOM!
//;---------------------------------------
scroll_back_top_irq:
		pha txa pha tya pha 
		
	//;	inc $d021

		asl $d019		
	
		lda scroll_verticale
		sta $d011
		sbc #1
		sta scroll_verticale
		
		cmp #$17
		beq !+
		jmp NOUPSCROLL
!:
		lda #$1f
		sta scroll_verticale

		sta $d011

		ldy #0
		//;Devo copiare tutte le righe all'insu'!
!:		
		lda $0400+40*13,y
		sta $0400+40*12,y
		
		lda $0400+40*15,y
		sta $0400+40*14,y
		
		lda $0400+40*16,y
		sta $0400+40*15,y
		
		lda $0400+40*17,y
		sta $0400+40*16,y
		
		lda $0400+40*18,y
		sta $0400+40*17,y
		
		lda $0400+40*19,y
		sta $0400+40*18,y

		lda $0400+40*20,y
		sta $0400+40*19,y
		
		lda $0400+40*21,y
		sta $0400+40*20,y
		
		lda $0400+40*22,y
		sta $0400+40*21,y
		
		lda $0400+40*23,y
		sta $0400+40*22,y
		
		lda $0400+40*24,y
		sta $0400+40*23,y

		iny		
		cpy #40		
		bne !-
		
	//;	inc delayer
	//;	lda delayer
	//;	cmp #5
	//;	bne NOUPSCROLL
		
		
NOUPSCROLL:
		
		inc vertical_scroller_fine_tune
		lda vertical_scroller_fine_tune
		cmp #34
		bne STILL_SCROLL

	//	lda #$1b
	//	sta $d011
		lda $d011
		and #%01111111
		sta $d011

		
		lda #$01
	//;	sta $d012

		lda #<fresh_tech_waiter_irq 
		sta $fffe
		lda #>fresh_tech_waiter_irq 
		sta $ffff
		
		lda #0
		sta delayer 		
		
STILL_SCROLL:		
		
		jsr $1003
	
		
		pla tay pla tax pla
		
		rti	


fresh_tech_waiter_irq:
		pha txa pha tya pha 
		asl $d019
		
	
		jsr $1003

		
//;		lda #1
//;		sta FINAL_STOP
		
		
		//;---------------------------
		//;INIT DI FRESHNESS
		jsr $3800
		
		//;; restituisce in A il raster line a cui impostare 
		//;l'irq e in Y,X Hi e Lo byte dell'handler
		jsr $3803
		sta TIME_IRQ_FRESH+1
		sta TIME_IRQ_FRESH_2+1
		
		sty HI_FRESH+1
		sty HI_FRESH_2+1
		
		stx LO_FRESH+1
		stx LO_FRESH_2+1

		lda #$01
		
		ldy #>pre_tech_irq
		ldx #<pre_tech_irq
		jsr $3806
		
	//;	lda #LIGHT_BLUE        //;Bordo su scroll
	//;	ldx #GRAY
	//;	jsr $3006 //;jsr setcolourfx
		
	
		/*
	//;	lda #BLUE  //;Bordo normale
	//;	ldx #BLACK //;Sfondo normale
	//;	jsr $3009 //;jsr setcolour
	*/
		lda #<scroll_text
		ldx #>scroll_text
		jsr $3809
		
		//;Colore sprites coprenti
		lda #BLACK
		sta $d027
		sta $d028
		
		//;testo scroller
		lda #LIGHT_GRAY
		sta $d029
		sta $d02a
		sta $d02b
		sta $d02c
		sta $d02d
		sta $d02e
		sta $d02f
	
		//;LDA BORDO EFFETTO  LDX BACKGROUND EFFETTO
		ldx #BLACK //Sfondo scroller
		lda #BLACK  //Bordo scroller
		jsr $380c
	    
		lda #BLACK  //Bordo normale
		ldx #BLACK //Sfondo normale
		jsr $380f
		
		//; ; Accumulatore: 0->no fx (background color), 1->square, 2->space, 3->both
		lda #1
		jsr $3812      
		
		inc CAN_START_FINAL_TECH
		
		lda CAN_START_FINAL_TECH
		cmp #1
		bne !+
		
				
		lda #<pre_tech_irq 
		sta $fffe
		lda #>pre_tech_irq 
		sta $ffff
		
		//;Sfilo la tabella d016 e d018 da li..
		
		ldx #0
LPX:
		
		lda d016_table,x
		sta $8000,x
		lda #$db
		sta d016_table,x
		
		lda d018_table,x
		sta $8100,x
		
		lda #$e8
		sta d018_table,x
		
		dex
		bne LPX
	
		
		
		pla tay pla tax pla
		rti	
		
		
!:
		
		pla tay pla tax pla
		rti	
		
.align $100
		
fade_intro_pt:
.byte $0

fade_intro_d021_pt:
.byte $6

fade_logo_pt:
.byte $0

delayer:
.byte $0

fade_table_intro:
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,GRAY,GRAY,GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
		.byte GRAY,GRAY,GRAY,LIGHT_GRAY,LIGHT_GRAY,GRAY,GRAY,GRAY
		.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,BLACK,BLACK,BLACK,BLACK,BLACK,BLACK,BLACK,BLACK,BLACK
		.byte 0
		
fade_table_logo:
		.byte $0,$0,$0,$0,$b,$b,$b,$b,$c,$c,$c,$c,$f,$f,$f,$f,$c,$c,$c,$c,$b,$b,$b,$b
		.byte $c,$c,$c,$c,$f,$f,$f,$f,$1,$1,$1,$1,$1,$1,$1,$1
				
scroll_verticale:
.byte $1b

offset_y:
.byte $0

fade_pointer_scritte:
.byte $0

delay_fade_scritte:
.byte $0

can_fade_scritte:
.byte $1

target_pt:
.byte $0

colori_target_lo:
.byte $83,$a0,$52,$15

colori_target_hi:
.byte $d8,$da,$db,$db

delayer_2:
.byte $0

credits_elder:
.text "elderoo1o"
.byte 0

credits_freshness:
.text "           freshness"
.byte 0

credits_almightygod:
.text "almightygod"
.byte 0

credits_psycho:
.text "          psycho858o"
.byte 0

INTRO_SUB_STATE:
.byte $0

fade_table_scroll_back:
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY,GRAY,GRAY,GRAY,GRAY
.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,CYAN,CYAN,CYAN,CYAN
.byte BLUE

fade_bordi_scroll:
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY,GRAY,GRAY,GRAY,GRAY
.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,WHITE,WHITE,WHITE,WHITE
.byte LIGHT_GRAY

fade_striscia_scroll:
.byte BLUE,BLUE,BLUE,BLUE,LIGHT_BLUE,LIGHT_BLUE,LIGHT_BLUE,LIGHT_BLUE
.byte WHITE,WHITE,WHITE,WHITE,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte DARK_GRAY

scroller_fader_pt:
.byte $0

CAN_START_CHECKER:
.byte $0

CAN_START_FINAL_TECH:
.byte $0

CAN_SWING_TECH:
.byte $0

vertical_scroller_fine_tune:
.byte $0

h_scroller_1:
.byte $0

h_scroller_2:
.byte $0


