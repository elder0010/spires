//;----------------------------------------------------------
//;----------------------------------------------------------
//;				Spires Elder0010 / Freshness - Onslaught
//;----------------------------------------------------------
//;----------------------------------------------------------

.var COL_SPR1 = $D027

.var src_lo = trch +1
.var src_hi = trch +2
.var dst_lo = trch +1+3
.var dst_hi = trch +2+3

.var app = $03             
.var cnt = $028 

//;Intro
		lda #0
		sta $d015
		
		//;Pulisco la scritta commodore originale
		ldx #0
		lda space
!:
		sta $0431,x
		inx
		cpx #21
		bne !-
		
		ldx #0
		
		
		//;Punto gli sprites
		ldy #[sprite1/$40]
setsprite_pntr:
        tya
        sta sprite_pntr,x
        lda #1
        sta COL_SPR1,x
        iny
        inx
        cpx #8
        bne setsprite_pntr
		
		
		
		lda #$0
		sta pt_d016
		sta pt_d018
		sta loop_counter
		sta sprite_speed
		sta delay
	
		
		//;sta $d015
		
		///; stop nmi from causing flicker 
		lda #<nmi sta $fffa
		lda #>nmi sta $fffb
		
	//	lda #$6
	//	sta $d020
		
		lda #$e
		//sta $d021
		
		ldx #$0
		ldy #0

		//;---------------------------
		//;Breaker intro

		lda #$b
		sta $d027
		lda #1
		sta $d028
		lda #2
		sta $d029
		lda #3
		sta $d02a
		lda #$8
		sta $d02b
		lda #5
		sta $d02c
		lda #$b
		sta $d02d
		lda #$d
		sta $d02e
		
		//;COLORI UGUALI
		lda #LIGHT_BLUE
		sta $d027
		sta $d028
		sta $d029
		sta $d02a
		sta $d02b
		sta $d02c
		sta $d02d
		sta $d02e
		sta $d02f
		
		//;Posiziono gli sprite
		ldx #96
		ldy #58
		stx $d000
		sty $d001
		sty $d003
		sty $d005
		sty $d007
		sty $d009
		sty $d00b
		sty $d00d
		sty $d00f
		txa
		
		adc #23
		sta $d002
		adc #24
		sta $d004
		adc #24
		sta $d006
		adc #24
		sta $d008
		adc #24
		sta $d00a
		adc #24
		sta $d00c
		adc #24
		sta $d00e
	
		lda #0
        sta cnt
        sta app
	
		lda #$ff
		sta $d015
		
		//;Scrivo il testo sugli sprites
		lda #<sprite1
        sta dst_lo
        lda #>sprite1
        sta dst_hi
		
		lda #$33
        sta $01
lp1:
        lda #0
        sta pnt_1
	
        ldy cnt
        lda texts,y
		

        asl
        rol pnt_1
        asl
        rol pnt_1
        asl
        rol pnt_1

        sta src_lo
        lda pnt_1
        clc          
        adc #>$d800  
        sta src_hi
		
		
	
        ldy #0
        ldx #0
trch:
        lda $ffff,y
        sta $ffff,x
        inx
        inx
        inx

        iny
        cpy #8
        bne trch


        inc dst_lo
        inc app
        lda app
        cmp #3
        bne noincdestpr

        lda #0
        sta app
        lda dst_lo
        clc
        adc #$3d
        sta dst_lo
        bcc noincdestpr

        inc dst_hi

noincdestpr:
        inc cnt
        lda cnt
        cmp #21
        bne lp1
		