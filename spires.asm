//;----------------------------------------------------------
//;----------------------------------------------------------
//;				Spires Elder0010 / Freshness - Onslaught
//;----------------------------------------------------------
//;----------------------------------------------------------
.var base = $8000
.var codef = $1000
.var outro_pnt = $40

.var top_final_scroller = $4c7
.var bottom_final_scroller = $680+$28

.pc = $2f80 "sprites scroller finale"
.import binary "tech_chars/scroller_end_sprites.raw"

//----------------------------------------------------------
//;.var music = LoadSid("Turbulent_Times.sid")
//;.var music = LoadSid("tech_chars/psycho.prg")
.pc = $1000
.import c64 "tech_chars/psycho.prg"
//----------------------------------------------------------
//;.pc=music.location "Music" .fill music.size, music.getData(i)
//----------------------------------------------------------
.pc = * "lut logo semplice"
LUT_LOGO_SEMPLICE:
.import c64 "tech_chars/logo_liscio/s.prg"
//----------------------------------------------------------



.var BACKUP_D016 = $db

.pc = * "ZONA FREE"
//;Irq "Direttore del tech

delay_fadeout_tech:
.byte $0

delay_fadeout_tech_2:
.byte $0

irq_top:	
		:irq_open()

		asl $d019				   			   			   			

		lda $d011 
		and #%01111000 
		ora #%00000011
		sta $d011
		
		lda #$2e
		sta $d012
		
		lda $DD00
		and #%11111100
		ora #%00000010
		sta $DD00
		
		
		lda #<raster1
		ldx #>raster1
		
		sta $fffe
		stx $ffff
				
		lda CAN_SWING_TECH
		cmp #3
		bne !+
		jmp FADE_OUT_TECH
!:
		cmp #4
		bne !+
		jmp FADE_REAL_TECH
!:

		cmp #1
		beq SWING_TECH
		
		cmp #2
		beq SWING_INSIDE_TECH
		
		//;Qui � prima dello swing :)
	
		ldx fade_pt
		lda techer_fader_1,x
		sta $d022
		lda techer_fader_2,x
		sta $d023
		inc fade_pt
	//;	cpx #18
		cpx #31
		beq AHE
		jmp EXIT_DIRECTOR_IRQ
AHE:
		
		
		lda #1
		sta CAN_SWING_TECH

SWING_TECH:		

		inc FINAL_DELAYER
		lda FINAL_DELAYER
		cmp #$ff
		beq DLL
		
		jmp EXIT_DIRECTOR_IRQ
DLL:
		
		lda #$fe
		sta FINAL_DELAYER
		
		inc FINAL_DELAYER_2
		lda FINAL_DELAYER_2
		cmp #$95
		beq DLL_2
		
		jmp EXIT_DIRECTOR_IRQ
DLL_2:

		
		lda #$fe
		sta FINAL_DELAYER
		lda #$95-1
		sta FINAL_DELAYER_2
		
		ldx scroller_fader_pt
		lda $8000,x
		sta d016_table,x
		lda $8100,x
		sta d018_table,x
		
		dex
		
		ldx scroller_fader_pt
		lda $8000,x
		sta d016_table,x
		lda $8100,x
		sta d018_table,x
		
		dec scroller_fader_pt
		dec scroller_fader_pt
		
		lda #20
		sta start_offset+1
	
	//;	dec start_offset+1
		
		ldx scroller_fader_pt
		bne !+
		
		lda #2
		sta CAN_SWING_TECH
SWING_INSIDE_TECH:
		ldx scroller_fader_pt
		lda $8000,x
		sta d016_table,x
		lda $8100,x
		sta d018_table,x

		dec scroller_fader_pt

		lda #20
		sta start_offset+1	
		
		lda scroller_fader_pt
		cmp #0
		bne !+
		
		lda #3
		sta CAN_SWING_TECH
!:
EXIT_DIRECTOR_IRQ:
		
		jmp MUZIK
		//;Fadeout del tech
		
FADE_OUT_TECH:
		inc delay_fadeout_tech
		lda delay_fadeout_tech
		//;cmp #$60
		cmp #$6a
		bne MUZIK
		lda #0
		sta delay_fadeout_tech
		inc delay_fadeout_tech_2
		lda delay_fadeout_tech_2
		cmp #14
		bne MUZIK
		
		lda #0
		sta $2f //;fermo lo scroll
		lda #4
		sta CAN_SWING_TECH
		lda #18
		sta fade_pt
		jmp MUZIK
FADE_REAL_TECH:
		//;FADEOUT DEL TECH!! per davvero eh :D
		
		ldx fade_pt
		lda techer_fader_1,x
		sta $d022	
		lda techer_fader_2,x
		sta $d023
		
		txa
		tay
		ldx #BLACK
		lda fade_striscia_scroll,y
		jsr $380c
		
		lda fade_table_scroll_back,y
		ldx #BLACK //Sfondo normale
		jsr $380f


		ldx fade_pt
		dec fade_pt
		cpy #0
		bne MUZIK
	
	//;	lda #18
	//;	sta fade_pt


		lda #<end_demo_irq
		ldx #>end_demo_irq
		
		sta $fffe
		stx $ffff
		
		lda #$ff
		sta $d012
		
		lda #0
		sta $d020
		sta $d021
		sta mini_fader_pt
	
		lda #$b
		sta $d011
		
		lda $DD00
		and #%11111100
		ora #%00000011
		sta $DD00
	
		lda #$c8
		sta $d016
		jsr clear_screen_half
		jsr color_screen_half
MUZIK:

		
		jsr $1003
		
		
		lda CAN_SWING_TECH
		cmp #3
		bne SKIPF
		
		//;Vediamo il blinko
		lda $177c
		cmp #$41
		beq STANDARD_COL
		
		//;Blinko
	//;	lda #RED
	//;	ldx #BLACK //Sfondo normale
		lda #1
		sta can_mini_fade
		jmp BLINK
	
STANDARD_COL:
	//;	lda #DARK_GRAY
	//;	ldx #BLACK //Sfondo normale
BLINK:
	//;	jsr $380f
		
		
		lda can_mini_fade
		cmp #1
		bne NO_MINIF

		
		ldx mini_fader_pt
		lda mini_fader_music,x
		ldx #BLACK
		jsr $380f
		
		inc mini_fader_pt
		lda mini_fader_pt
		cmp #61
		bne NO_MINIF
		lda #0
		sta mini_fader_pt
		sta can_mini_fade	
		
		
SKIPF:	
	
NO_MINIF:
		
		pla tay pla tax pla
	
		rti



.pc = * "scroller text"


final_delayer:
.byte $0


draw_logo_petscii:
	//;lda #$64
		ldx #0
!:
		lda petscii_lut,x
		sta $400+$000,x
		lda petscii_lut+$100,x
		sta $400+$100,x
		lda petscii_lut+$200,x
		sta $400+$200,x
		lda petscii_lut+$300,x
		sta $400+$300,x

		dex 
		bne !-
	
		//;E creo charset farlocco
		lda #$00
		ldx #0
!:
		sta $2800,x
		inx
		cpx #9
		bne !-
	
		ldx #0
		lda #$ff
!:
		sta $2808,x
		inx
		cpx #9
		bne !-
	
		rts
	
color_screen_half:

		lda #$f
		ldx #0
	!:
		sta $d800,x
		sta $d800+$100,x
		sta $d800+$200,x
		sta $d800+$300,x
		dex
		bne !-
		rts
	



.macro inc_line(register){
		clc
		lda register+1
		adc #$28
		sta register+1
		bcc !+
		inc register+2
!:
}
	
//----------------------------------------------------------
.pc = $7800 "lut tech"
.import binary "tech_chars/lut.raw"

.pc = $7c00 "lut petscii"
petscii_lut:
.import binary "tech_chars/petscii.raw"

.pc = $8000 "sprite fpp routine"
.import c64 "tech_chars\freshzoom.prg"
.pc = * "Lut di freshness"
.fill $3ff,0

/*
.pc = $f000 "sprite fpp routine"
.import c64 "spritezoom.exo"
*/
endfpp:

.var SCREEN_1 = $7800
.var SCREEN_2 = $f800

.var SPRITES_START = $2c00
.var sprite_pntr= $07f8
.var pt_d018 = $13
.var pt_d016 = $12
.var loop_counter = $14
.var pointer = $15
.var sprite_speed = $16
.var delay = $17

.var pnt_1 = $02              
    
	//;	:BasicUpstart2(init)

SCENE_STATE:
.byte $0


.pc = $0850 "init"
init:
		sei
		
	//;	lda #music.startSong-1 
	//;	jsr music.init
		
		lda #0
		sta outro_pnt
		jsr $1000
		
		//;turn off cia
		lda #$7f sta $dc0d  
		lda $dc0d sta $dd0d lda $dd0d
		lda #$01 sta $d01a
		
	//;	lda #$35
	//;	sta $01
		
//;	jmp TECH_INIT
		
		//;Preparo gli sprites e le cose per l'intro
		.import source "spires_intro.asm"
		
		lda #$35
		sta $01
		
		//;punto il fade irq
		lda #<fade_irq sta $fffe
		lda #>fade_irq sta $ffff

		lda #$32 
		sta $d012
		
		lda #$1b
		sta $d011
		
		
		cli
		
		//;Intro breaker
!:
		lda SCENE_STATE
		cmp #0
		beq !-
		
		ldy #0
		lda space
		
!:
		sta $0400,y
		sta $0400+$100,y
		sta $0400+$200,y
		sta $0400+$300,y
		dey
		bne !-
		

		//;Fade irq
!:
		lda SCENE_STATE
		cmp #1
		beq !-	
	
		
		//;Disegno i COLORI
		ldx #$0
!:		
		lda #$0
		sta $d800,x
		lda #$b
		sta $d800+$100,x
		sta $d800+$200,x
		lda #$0
		sta $d800+$260,x
		sta $d800+$300,x
		dex
		bne !-
!:

SKIP_LOGO:
		
		//;Disegno i crediti
		ldy #0
		ldx #0
		
!:		
		lda credits_elder,y
		cmp #0
		beq fine_elder
		sta $0400+$28*1,x
		adc #63
		sta $0401+$28*1,x
		adc #64
		sta $0400+$28*1+$28,x
		adc #64
		sta $0401+$28*1+$28,x

		inx
		inx
	
		iny
		jmp !-
	
fine_elder:

		
		ldy #0
		ldx #0
		
!:		
		lda credits_freshness,y
		cmp #0
		beq fine_freshness
		sta $0400+$28*4,x
		adc #63
		sta $0401+$28*4,x
		adc #64
		sta $0400+$28*4+$28,x
		adc #64
		sta $0401+$28*4+$28,x

		inx
		inx
	
		iny
		jmp !-


fine_freshness:
		
		ldy #0
		ldx #0
		
!:		
		lda credits_almightygod,y
		cmp #0
		beq fine_almightygod
		sta $0400+$28*18,x
		adc #63
		sta $0401+$28*18,x
		adc #64
		sta $0400+$28*18+$28,x
		adc #64
		sta $0401+$28*18+$28,x

		inx
		inx
	
		iny
		jmp !-

fine_almightygod:	
		
	
		ldy #0
		ldx #0
		
!:		
		lda credits_psycho,y
		cmp #0
		beq out_credits_1
		sta $0400+$28*22,x
		adc #63
		sta $0401+$28*22,x
		adc #64
		sta $0400+$28*22+$28,x
		adc #64
		sta $0401+$28*22+$28,x

		inx
		inx
	
		iny
		jmp !-	
		
out_credits_1:		
		
!:
		lda SCENE_STATE
		cmp #2
		beq !-	
		
		
		lda #0
		sta $d022

		//;Attendo che i crediti siano visualizzati
		//;Disegno il logo
		ldx #0
!:
		
		lda LUT_LOGO_SEMPLICE+$200,x
		sta $0400+$200-$28*5,x
		lda LUT_LOGO_SEMPLICE+$220,x
		sta $0400+$220-$28*5,x
	
		dex
		bne !-
		
		
!:
		lda SCENE_STATE
		cmp #3
		beq !-	
		
	
		//;Disegno i colori sulla seconda meta dello schermo
		//;altrimenti quando scrollo il logo si spacca il mondo!
		ldx #0
!:	

		lda #$c0
		sta $0400,x
		sta $0400+$280,x
		sta $0400+$300,x
		
		
		lda #$b
		sta $d800,x
		sta $d800+$100,x
		sta $d800+$200,x
		sta $d800+$300,x

		inx
		bne !-
		
!:		
		lda SCENE_STATE
		cmp #4
		beq !-
		
//;---------------------------
//;INIT DEL CHECKER!
//;---------------------------	
	//;	inc $d020

		ldy #$08
		jsr $9000
				
		ldy #$00
		ldx #$00
		jsr $9000+12

		ldy #$00
		ldx #$00 
		jsr $9000+15

		jsr $9000+3
		inc CAN_START_CHECKER

		jmp *
		
write_onslaught:
		ldy #0
		ldx #0
		
!:		
		lda onslaught_text,y
		cmp #0
		beq XX
		sta $0400+$16*21-16,x
		adc #63
		sta $0401+$16*21-16,x
		adc #64
		sta $0400+$16*21+$28-16,x
		adc #64
		sta $0401+$16*21+$28-16,x

		inx
		inx
	
		iny
		jmp !-	
XX:		
rts

//;Pulisce solo il fondo dello schermo
clear_screen_half:
		ldx #0
		lda space
!:
		sta $400+$000,x
		sta $400+$100,x
		sta $400+$200,x
		sta $400+$300,x
		dex 
		bne !-
	
rts

//;---------------------------
//;INIT DEL TECHER!
//;---------------------------		
TECH_INIT:
//;sei
		lda #$2e
		sta $d012
		lda #$1b
		sta $d011
		
		lda #<raster1 sta $fffe
		lda #>raster1 sta $ffff

		//; MAIN INIT
		lda #$ff
		sta $d015

		lda $DD00
		and #%11111100
		ora #%00000010
		sta $DD00
					
		lda #$e8
		sta $d018
			
ahead:
				
		
		ldx #72
		lda #$b
!:	
		sta $d800,x
		dex
		bne !-
		
		lda #0
		sta fade_pt
		sta $d022
		sta $d023

		lda #$1b
		sta $d011
		
		
		//;SETTO IL MIO IRQ!
		lda #$01	
		ldy #>irq_top
		ldx #<irq_top
		jsr $3806
				
	
		//; ; Accumulatore: 0->no fx (background color), 1->square, 2->space, 3->both
		lda #3
		jsr $3812      
		rts
		
		
		
.pc = * "end demo irqs"

//;Scroller finale
end_demo_irq:

		:irq_open()
		
		asl $d019	
		
		lda #$1c
		sta $d018
		
		jsr write_onslaught
		
		//;E punto pure gli sprites
		
		lda #0
		sta $d015
		
		lda #0
		
		sta $d017
		sta $d01b
		sta $d01d
		
		sta $d027
		sta $d028
		sta $d029
		sta $d02a
		
		lda #$be
		sta $0400+$03f8
		sta $0400+$03fa
		
		lda #$bf
		sta $0400+$03f9
		sta $0400+$03fb
	
		
		lda #%00001010
		sta $d010
		
		//;--asse x
		lda #$1f
		sta $d000
		sta $d004
		lda #$37
		sta $d002
		sta $d006
		
		//;--asse y
		lda #$55
		sta $d001
		sta $d003
		
		lda #$bf
		sta $d005
		sta $d007
		
		lda #%00001111
		sta $d015
	
		lda #$1b
		sta $d011
		
		lda #<bottom_end_scroller_irq 
		sta $fffe
		lda #>bottom_end_scroller_irq 
		sta $ffff
		
		lda #$b0
		sta $d012
		
	
		jsr $1003
	
		
		pla tay pla tax pla
	
		rti


		
//;top scroller
top_end_scroller_irq:
		:irq_open()
		
		asl $d019	
		
		lda #<center_end_scroller_irq 
		sta $fffe
		lda #>center_end_scroller_irq 
		sta $ffff
		
		lda #$18
TURN_OFF:
		and #%11111111
		sta $d011
		
		lda #$80
		sta $d012
		
		
		lda can_scroll_bottom
		cmp #1
		beq !+
		jmp noscroll
!:
		
		lda h_scroller_1
		sta $d016

		//;------------------
		//;Scroller INFERIORE
		//;------------------
		lda h_scroller_2
		sec
		sbc #$02
		and #$07 
		sta h_scroller_2
		bcs noscroll 
		
		lda second_half_char
		cmp #1
		beq feed_next_char
	//;	ldy final_scroller_pt_bottom
		
SOURCE_4:
		lda cheerz_text
		bne !+
		lda space
!:
		sta bottom_final_scroller+$27
		adc #64
		adc #64
		sta bottom_final_scroller+$28+$27
		
		inc second_half_char
		jmp cpl
feed_next_char:
		
	//;	ldy final_scroller_pt_bottom
SOURCE_3:
		lda cheerz_text
		cmp #0
		bne !+
		lda space
		
	
!:
		adc #63
		sta bottom_final_scroller+$27
		adc #128
		sta bottom_final_scroller+$28+$27
		
		dec second_half_char
	
		clc
		lda SOURCE_3+1
		adc #1
		sta SOURCE_3+1
		bcc !+
		inc SOURCE_3+2
		inc SOURCE_4+2
!:
		
		clc	
		lda SOURCE_4+1
		adc #1
		sta SOURCE_4+1
		
		
cpl:	
		ldx #$0
copyline:


		lda bottom_final_scroller+1,x
		sta bottom_final_scroller,x
		inx
		cpx #$28*2
		bne copyline
	
noscroll:	

		
pla tay pla tax pla
	
		rti
		

//;Bottom scroller
center_end_scroller_irq:
		:irq_open()
		
		asl $d019	
		
		lda #<bottom_end_scroller_irq 
		sta $fffe
		lda #>bottom_end_scroller_irq 
		sta $ffff
		
		lda #$1d
		sta $d011
		
		lda #$a5
		sta $d012
		
		lda #$c8
		sta $d016


		ldy #$21
!:		dey
		cpy #0
		bne !-
		
//;--------PRIMO RASTER-----------
		nop
		
RASTER_FINAL:
	//;	lda #$6
		lda #$0
		sta $d020
		sta $d021
		
		ldy #8
!:		dey
		cpy #0
		bne !-

SFONDO_FINAL:		
		//;lda #$b
		lda #$0
		sta $d020
		sta $d021
//;--------FINE PRIMO RASTER------
		ldy #$e6
!:		dey
		cpy #0
		bne !-
		
		nop
		nop
		

//;--------SECONDO RASTER---------
//;		lda #$6
RASTER_FINAL_2:
		lda #$0
		sta $d020
		sta $d021
		
		ldy #8
!:		dey
		cpy #0
		bne !-
		
		lda #0
		sta $d020
		sta $d021	
		
pla tay pla tax pla
	
		rti
		

//;Bottom scroller
bottom_end_scroller_irq:
		:irq_open()
		
		asl $d019	
		
		lda #<top_end_scroller_irq 
		sta $fffe
		lda #>top_end_scroller_irq 
		sta $ffff
		
		lda #$1a
		//;lda #$18

		sta $d011
		
		lda #$20
		sta $d012
		
		lda h_scroller_2
		sta $d016
		
	
		//;------------------
		//;Scroller SUPERIORE
		//;------------------
		
		lda can_scroll_top
		cmp #1
		beq !+
		jmp noscroll_2
!:
		lda h_scroller_1
		and #$07
		clc
K1:		adc #$02
		sta h_scroller_1
		cmp #8
		bne noscroll_2
		
		lda #0
		sta h_scroller_1
		
		clc
		lda second_half_char_2
		cmp #1
		beq feed_next_char_2
		
		//;Prima deve caricare la parte a DESTRA del char
SOURCE:
		lda cheerz_text_2-1
		cmp #0
		bne !+
		
		//;KILL SCROLLS!!
		lda #0
		sta K1+1
		inc can_fade_out_end
!:
		clc
		adc #64
		sta top_final_scroller
	
		adc #128
		sta top_final_scroller+$28

		inc second_half_char_2

		jmp cpl_2
		
feed_next_char_2:
		
		//;Poi deve caricare la parte a SX del char
		//;e incrementare lo scroll pointer
SOURCE_2:
		lda cheerz_text_2-1
		sta top_final_scroller
		
		
		adc #127
		sta top_final_scroller+$28

		
		sec
		lda SOURCE+1
		sbc #1
		sta SOURCE+1
	
		sec
		lda SOURCE_2+1
		sbc #1
		sta SOURCE_2+1
		
		bcs !+
		
		dec SOURCE+2
		dec SOURCE_2+2
!:
		dec second_half_char_2
		
cpl_2:	
		ldx #$27*2
copyline_2:

		lda top_final_scroller,x
		
		
		cmp #0
		bne !+
		lda #1
		sta can_fade_out_end //;faccio partire il mega fade finale

!:

		sta top_final_scroller+1,x
		
	//;	lda top_final_scroller+$27,x
	//;	sta top_final_scroller+1+$27,x
		dex
		cpx #$ff
		bne copyline_2
		
noscroll_2:	

		lda can_fade_final
		cmp #1
		bne NOFADEx
		//;Fade del finale
		ldx fade_pt_final_1
		lda fade_final_raster_back,x
		sta SFONDO_FINAL+1
		
		inc fade_pt_final_1
		lda fade_pt_final_1
		cmp #26
		bne !+
		dec can_fade_final
		inc can_scroll_top
		inc can_scroll_bottom
!:	
		ldx fade_pt_final_2
		lda fade_final_raster_linea,x
		sta RASTER_FINAL+1
		sta RASTER_FINAL_2+1
		
		inc fade_pt_final_2
		
			
NOFADEx:
		
		lda can_fade_out_end
		cmp #1
		bne THEMUSIC

		ldx fade_pt_final_1
		lda fade_final_raster_back,x
		sta SFONDO_FINAL+1
		
		ldx fade_pt_final_2
		lda fade_final_raster_linea,x
		sta RASTER_FINAL+1
		sta RASTER_FINAL_2+1
		
		dec fade_pt_final_1
		dec fade_pt_final_2
		
		lda fade_pt_final_1
		cmp #0
		bne THEMUSIC
		
		lda #$7b
		sta $d011
		
		jsr draw_logo_petscii
	
		lda #<outro_irq
		sta $fffe
		lda #>outro_irq
		sta $ffff
		
		lda #$25
		sta $d012
		
THEMUSIC:
		
		jsr $1003
		
		//;inc $680
		
		//;vediamo un attimo la cassa

	//;	bne NOCASSA
		
		lda #$2
		cmp $1787
		bne NOCASSA
		
		cmp $1788
		bne NOCASSA
		
		sta can_mini_fade
		
		inc botte_di_basso
		
NOCASSA:
		
		lda can_mini_fade
		cmp #2
		bne !+
		
		lda botte_di_basso
		cmp #7
		bmi !+
		
		cmp #13
		beq !+
		
		cmp #20
		bpl !+
		
		ldx mini_fader_pt
		lda mini_fader_music,x
		sta SFONDO_FINAL+1
		inc mini_fader_pt
		lda mini_fader_pt
		cmp #11
		bne !+
		lda #0
		sta mini_fader_pt
		sta can_mini_fade	
!:
		
		pla tay pla tax pla
	
		rti
		

final_scroller_pt_bottom:
.byte $0	

final_scroller_pt_top:
.byte $ff

second_half_char:
.byte $0

second_half_char_2:
.byte $0

can_scroll_top:
.byte $0

can_scroll_bottom:
.byte $0

can_fade_final:
.byte $1



//;---------------------------------------
//;--INTRO PRE TECH TECH
//;---------------------------------------
//;questo sta in top $01
.pc = * "Intro pre tech tech"
pre_tech_irq:
		pha txa pha tya pha 

		asl $d019
		
		//;lda #$da
		lda #BACKUP_D016
		sta $d016
		
		lda $DD00
		and #%11111100
		ora #%00000011
		sta $DD00

		lda #$1a
		sta $d018
		
		jsr $1003
		
		lda #<bottom_pre_tech_irq 
		sta $fffe
		lda #>bottom_pre_tech_irq 
		sta $ffff
		
		lda #$f8
		sta $d012
		
		lda	#$1b
		sta $d011
		
		inc PRE_TECH_FADER_DELAY
		
		lda PRE_TECH_FADER_DELAY
		cmp #$29
		beq ENTER_FADE
		
		jmp END_IRQXZ
ENTER_FADE:

		lda #$28
		sta PRE_TECH_FADER_DELAY
		
		//;Se l'intro � finito (il fade!) allora jump direttamente a dopo
		lda INTRO_SUB_STATE
		cmp #3
		bne !+
		jmp AFTER_FADE
!:
		
		//;E vai con l'intro
		inc delayer
		lda delayer
		cmp #0
		bne !+
		inc INTRO_SUB_STATE
	//;	inc $d020
!:
		lda INTRO_SUB_STATE
		cmp #2
		bne !+
		
//;------------------------------
//;-----FADE DELLO SCROLL E BORDI
//;------------------------------
		lda #1
		sta delayer
		
		ldy scroller_fader_pt
		lda fade_bordi_scroll,y
		sta BRD+1
		
		lda fade_table_scroll_back,y
		sta SFONDO_SCRL+1
SFONDO_SCRL:		
		ldx #BLUE //Sfondo scroller

BRD:
		lda #BLACK  //Bordo scroller
		sta $d027
		sta $d028
		jsr $380c

		ldy scroller_fader_pt
BORDO_NORMALE:		
	
		lda fade_striscia_scroll,y  //Bordo normale
		ldx #BLACK //Sfondo normale
		jsr $380f
		
		inc scroller_fader_pt
		lda scroller_fader_pt
		cmp #17 
		bne !+
		lda #0
		sta scroller_fader_pt
		lda #3
		sta INTRO_SUB_STATE
!:
		jmp END_IRQXZ
AFTER_FADE:
		jsr TECH_INIT

END_IRQXZ:
		pla tay pla tax pla
		
		rti	

//;questo apre il bordo a $f9
bottom_pre_tech_irq:
		pha txa pha tya pha 

		asl $d019	
		
	//;	lda #5
	//;	sta $d021
		
		
		//;facciamo partire il bordo inferiore
		//;lda #$13
		lda $d011 and #%11110111 sta $d011
		sta $d011
				
		lda #$10
	//;	sta $d018
		
		//;restituiamo il banco a fresh..
		lda #$97
	//;	sta $dd00
		
		lda #$ff
		sta $d015
		
		//;Punto l'irq di fresh!
TIME_IRQ_FRESH_2:		
		lda #$fe 
		sta $d012
		
		//;---PUNTO IRQ DI FRESHNESS----
HI_FRESH_2:
		lda #$15
		sta $fffe
LO_FRESH_2:
		lda #$30 
		sta $ffff
		
		
		pla tay pla tax pla
		
		rti	



		
nmi:	rti

texts:
	.text "COMMODORE 64 BASIC V2"

space:
	.text " "


	
.pc = $3000 "charset_scroller"
.import c64 "tech_chars\spiresfixy3.prg"
.pc = $3800 "freshness routine"
.import c64 "sc3800.prg"

.pc = $b400	"code"
code:	
	
.align $100	
.pc = * "irq tech tech"
raster1:

		:irq_open()

		asl $d019				   			   			   			
		lda $d011 and #%01111111 sta $d011
TIME_IRQ_FRESH:		
		lda #$fe 
		sta $d012
		
		//;---PUNTO IRQ DI FRESHNESS----
HI_FRESH:
		lda #$15
		sta $fffe
LO_FRESH:
		lda #$30 
		sta $ffff
		//;------------------------------
	
		
		//; give stable raster
		ldx $d012
		inx
		cpx $d012
		bne * - 3
		ldy #$0a
		dey
		bne * - 1
		inx
		cpx $d012
		nop
		beq Time_1
		nop
		bit $24
Time_1:	
		ldy #$09
		dey
		bne * - 1
		nop
		nop
		inx
		cpx $d012
		nop
		beq Time_2
		bit $24
Time_2:	
		ldy #$0a
		dey
		bne * - 1
		inx
		cpx $d012
		bne Time_3
Time_3:	
		nop
	
		 //; stable raster here
		ldy #$9
		nop
		:wait()
		
start_offset:

		ldx #$0
			
loop_r:		
		
		:fill_line()
		inx
		nop
		
		:fill_line()
		inx
			
		ldy #8
		:wait()
		nop
		nop
	
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
				
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
	
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		
		ldy #5
		:wait()
		nop
		nop
		
		inc loop_counter
		lda loop_counter
		cmp #19
		
	
		beq !+

		jmp loop_r
!:

	
		//;SPLIT SCREEN!
	//;	lda #$94 //;WAHO! VA UGUALMENTE!
		//;and #0
		sty $dd00
	
loop_r2:		
		
		:fill_line()
		inx
		nop
		
		:fill_line()
		inx
		
		
		
		ldy #8
		:wait()
		nop
		nop
	
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
				
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
	
		ldy #8
		:wait()
		nop
		nop
		
		:fill_line()
		inx
		
		
		ldy #5
		:wait()
		nop
		nop
		
		inc loop_counter
		lda loop_counter
		cmp #25
		beq !+

		jmp loop_r2
!:

		
		//;INIT IRQ DI FRESHNESS E VARIE
		lda #$13
		sta $d011
		
		
		lda #$10
		sta $d018
		
		//;restituiamo il banco a fresh..
		lda #$97
		sta $dd00
		
		
		inc start_offset+1
		inc start_offset+1

		lda #0
		sta loop_counter	
    
		lda #$ff
		sta $d015

		pla tay pla tax pla
	
	
		rti
	
		
table_loop_counter:
		.byte $0

PRE_TECH_FADER_DELAY:
	.byte $0
	
FINAL_DELAYER:
	.byte $0
	
FINAL_DELAYER_2:
	.byte $0
	
.align $100
.pc =* "tabella techtech d016"		

d016_table:
		
		:andata_d016()
	
		

.align $100
.pc = * "tabella techtech d018"		
d018_table:
		:andata_d018()

.macro andata_d016(){
	.byte $df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$de,$de,$de,$de,$de,$dd,$dd,$dd,$dd,$dd,$dc,$dc,$dc,$db,$db,$db,$db,$da,$da,$da,$d9,$d9,$d8,$d8,$d8,$df,$df,$de,$de,$de,$dd,$dd,$dc,$dc,$dc,$db,$da,$da,$da,$d9,$d9,$d8,$d8,$df,$df,$de,$de,$dd,$dd,$dd,$dc,$db,$db,$da,$da,$d9,$d9,$d8,$d8,$d8,$df,$df,$de,$de,$dd,$dc,$dc,$dc,$db,$db,$da,$da,$da,$d9,$d9,$d8,$d8,$d8,$df,$df,$de,$de,$de,$dd,$dd,$dd,$dc,$dc,$dc,$db,$db,$db,$db,$da,$da,$da,$d9,$d9,$d9,$d9,$d9,$d9,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d8,$d9,$d9,$d9,$d9,$d9,$d9,$da,$da,$da,$da,$db,$db,$db,$dc,$dc,$dc,$dc,$dd,$dd,$dd,$de,$de,$de,$df,$df,$d8,$d8,$d9,$d9,$d9,$da,$da,$db,$db,$db,$dc,$dc,$dd,$dd,$de,$de,$df,$df,$d8,$d8,$d9,$d9,$da,$da,$db,$db,$dc,$dc,$dc,$dd,$dd,$de,$df,$df,$d8,$d8,$d9,$d9,$d9,$da,$da,$db,$db,$dc,$dc,$dc,$dd,$de,$de,$de,$df,$df,$d8,$d8,$d8,$d9,$d9,$d9,$da,$da,$da,$db,$db,$db,$dc,$dc,$dc,$dc,$dd,$dd,$dd,$dd,$de,$de,$de,$de,$de,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df,$df
	}
	
.macro andata_d018(){
	.byte $ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e0,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e2,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$e8,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ea,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec,$ec
	}

fade_table:
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,GRAY,GRAY,GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
		.byte GRAY,GRAY,GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_BLUE,LIGHT_BLUE,LIGHT_BLUE
		.byte BLUE
fade_pt:
		.byte $0
first_irq_state:
		.byte $0
		
first_irq_delay:
		.byte $0
		
	fade_final_raster_linea:
.byte WHITE,WHITE,WHITE,WHITE,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte GRAY,GRAY,GRAY,GRAY
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
.byte BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE

fade_final_raster_back:
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
.byte GRAY,GRAY,GRAY,GRAY
.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte WHITE,WHITE,WHITE,WHITE
.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte GRAY,GRAY,GRAY,GRAY
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY

fade_pt_final_1:
.byte $0

fade_pt_final_2:
.byte $0

		
//;--------------------------------------------
//;---Intro routine----------------------------
//;--------------------------------------------
.pc = * "intro irq"
	.import source "spires_first_scene.asm"
	



//;

		


.pc = $2800 "Charset logo semplice"
.import c64 "tech_chars/logo_liscio/f.prg"
.align $40
.pc = * "filler bytes"
.fill 64,$0

scroll_text:
//;.text "               .....elder0010 and freshness are proud to present their first co op demo! "
.text "               ...preparing artificial biosphere... breeding zero oxygen lifeform to resist the separation between human nature from technology... converging to spires..."
.byte 0

final_color_fader:

.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY,GRAY,GRAY,GRAY,GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte WHITE,WHITE,WHITE,WHITE,WHITE,WHITE,WHITE,WHITE
.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
.byte GRAY,GRAY,GRAY,GRAY
.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY,BLACK

.align $40
.pc = SPRITES_START "sprites intro"
sprite1:
        .fill $40,$0
sprite2:
        .fill $40,$0
sprite3:
        .fill $40,$0
sprite4:
        .fill $40,$0
sprite5:
        .fill $40,$0


//;-------------------BANK1-----------------
.pc = $4000 "charset_11"
char_dst:
.import binary "tech_chars/chs00.raw"

.pc = $4800 "charset_12"
.import binary "tech_chars/chs01.raw"

.pc = $6000 "charset_13"
.import binary "tech_chars/chs02.raw"

.pc = $6800 "charset_14"
.import binary "tech_chars/chs03.raw"

.pc = $7000 "charset_15"
.import binary "tech_chars/chs04.raw"

//;-----------------------------------------

//;temp charsets
/*
;.pc = $8000 "temp charset (3 charsets)"
;.import binary "tech_chars/chs12.raw"
;.import binary "tech_chars/chs13.raw"
;.import binary "tech_chars/chs14.raw"

//;Questo � l'unico temp charset rimanente. andr� decompresso 
//;a $f000
;.pc = $ac10 "temp charset"
;.import binary "tech_chars/chs14.exo"
*/


//;-------------------BANK2-----------------
.pc = $c000 "charset_21"
char_dst2:
.import binary "tech_chars/chs10.raw"

.pc = $c800 "charset_22"
.import binary "tech_chars/chs11.raw"


.pc = $e000 "charset_23"
.import binary "tech_chars/chs12.raw"

.pc = $e800 "charset_24"
.import binary "tech_chars/chs13.raw"

.pc = $f000 "charset_25"
.import binary "tech_chars/chs14.raw"

.pc = $f800 "schermo_2"
.import binary "tech_chars/lut.raw"
//;-----------------------------------------
/*
.pc = $b000	"lut"	
table_dst:
.import binary "tech_chars/lut.raw"
*/
.pc = $b000 "mostro_finale"
//;---------FADE DEL MOSTRO FINALE------	

outro_irq:
		:irq_open()
		
		asl $d019	
		
		lda #0
		sta $d015
		
			
		lda #$7b
		sta $d011
		
		
		
		lda #$25
		sta $d012
		
		lda #$2b
		sta $d018
		
		
		lda #1
		sta $d800
		
		lda can_final_fade
		cmp #1
		beq FADEGO
		
		inc final_delayer
		lda final_delayer
		cmp #$10
		bne NOFADE
		
	
		
		inc can_final_fade
FADEGO:		
		lda #$1a
		sta $d018
		
		lda #$93
		sta $d011
		
		ldx wobble_pnt

		lda final_color_fader,x
		sta $d020
		sta $d021

		inc wobble_pnt

		lda wobble_pnt
		cmp #33
		bne NOFADE
		
	
		lda #0
		sta wobble_pnt
		sta can_final_fade
		sta $d020
		sta $d021
		
		lda #<kill_demo_irq
		sta $fffe
		lda #>kill_demo_irq
		sta $ffff
		
NOFADE:
		
		jsr $1003
			
		pla tay pla tax pla
	
		rti

kill_demo_irq:
		:irq_open()
		
		asl $d019
		
		lda #$8b
		sta $d011
		
		
		//;jsr $1003
			
		pla tay pla tax pla
	
		rti

.pc = * "fade_row_outro"
fade_row_outro:
.byte $4,$18,$a,$1e,$1,$1b,$8,$20,$15,$5,$2,$18,$a,$1e,$1,$1b,$8,$20,$15,$3
.byte $4,$18,$a,$1e,$1,$1b,$8,$20,$15,$5,$2,$18,$a,$1e,$1,$1b,$8,$20,$15,$3

ticker_red:
.byte $0

wobble_pnt:
.byte $0

can_final_fade:
.byte $0

can_fade_out_end:
.byte $0

//;32 colours
techer_fader_1:
		.byte BLACK,BLACK,BLACK,BLACK
		.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
		.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
		.byte GRAY,GRAY,GRAY,GRAY
		.byte GRAY,GRAY,GRAY,GRAY
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
		.byte WHITE,WHITE,WHITE,WHITE

//;32 colours	
techer_fader_2:
		.byte BLACK, DARK_GRAY,DARK_GRAY
		.byte DARK_GRAY,DARK_GRAY,DARK_GRAY,DARK_GRAY
		.byte GRAY,GRAY,GRAY,GRAY
		.byte GRAY,GRAY,GRAY,GRAY
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
		.byte LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY,LIGHT_GRAY
		.byte CYAN,CYAN,CYAN,CYAN
		.byte CYAN,CYAN,CYAN,CYAN
		.byte BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE,BLUE



mini_fader_music:
.byte GRAY,GRAY,LIGHT_GRAY,LIGHT_GRAY,WHITE,WHITE,LIGHT_GRAY,LIGHT_GRAY,GRAY,GRAY,DARK_GRAY
.fill 80,DARK_GRAY
mini_fader_pt:
.byte $0

can_mini_fade:
.byte $0

botte_di_basso:
.byte $0

botte_di_flash:
.byte $0

.pc = * "credits txt"
.text "                 "
cheerz_text:
.text "level 64 mahoney maniacs of noise mdg no name noice nostalgia offence origo dreamline oxyron padua panda design plush prosonix raffox resource role samar singular success trc the dreams the force the mean team triad trsi vibrants vision wow wrath designs xenon                   "
.byte 0
.text "                  "
.pc = * "cred 2 start"
.text "                                                          "
.byte 0
cheerz_text2_start:
.text "                   arise armageddon arsenic avatar beyond force black sun bonzai booze design byterapers camelot chorus chrome cosine crest cygnus oz defame dekadence disaster area elysium extend fairlight flach inc focus genesis project hack n trade hokuto force ian coog laxity"
.pc = * "cred 2 end"
cheerz_text_2:


onslaught_text:
.text "onslaught 2012"
.byte 0	

.macro irq_open(){
	pha txa pha tya pha
}

.macro irq_close(){
	pla tay pla tax pla
}

.macro fill_line(){
		lda d016_table,x
		sta $d016
		lda d018_table,x
		sta $d018
}

.macro wait(){
		dey
		bne *-1
}


